//
//  Game.cpp
//  Game-mac
//
//  Created by Sanjay Madhav on 5/31/17.
//  Copyright © 2017 Sanjay Madhav. All rights reserved.
//

#include "Game.h"
#include "SDL/SDL.h"
using namespace std;

// TODO

Game::Game()
{
	
}

bool Game::Initialize()
{
	//Initiliaze video and audio
	SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO);

	lastFrame = (SDL_GetTicks() / 1000);
	deltaTime = 0;

	mBallVelocity.x = 150;
	mBallVelocity.y = 150;
	mMoveDir = 0;
	mRunning = true;
	mWallThickness = 15;
	mPaddlePos.x = 40;
	mPaddlePos.y = 768 / 2;
	mBallPos.x = 1024 / 2;
	mBallPos.y = 768 / 2;

	//Create SDL window
	SDL_Window *window;

	window = SDL_CreateWindow(
		"SDL window",
		SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED,
		1024,
		768,
		SDL_WINDOW_OPENGL
	);

	mWindow = window;

	//Create SDL renderer
	SDL_Renderer *renderer;

	renderer = SDL_CreateRenderer(
		window,
		-1,
		SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC
	);

	mRenderer = renderer;

	return true;
}

void Game::Shutdown()
{
	SDL_DestroyRenderer(mRenderer);
	SDL_DestroyWindow(mWindow);
	SDL_Quit();
}

void Game::RunLoop()
{
	while (mRunning)
	{
		ProcessInput();

		UpdateGame();

		GenerateOutput();

	}

}

void Game::ProcessInput()
{
		SDL_Event event;
		while (SDL_PollEvent(&event)) {

			switch (event.type)
			{
			case SDL_QUIT:
				mRunning = false;
				break;
			}
		}

		//Get keyboard state
		const Uint8 *state = SDL_GetKeyboardState(NULL);
		if (state[SDL_SCANCODE_ESCAPE])
		{
			mRunning = false;
		}

		if (state[SDL_SCANCODE_UP])
		{
			mMoveDir = 1;
		}

		else if (state[SDL_SCANCODE_DOWN])
		{
			mMoveDir = -1;
		}

		else if ((!state[SDL_SCANCODE_DOWN]) && (!state[SDL_SCANCODE_UP]))
		{
			mMoveDir = 0;
		}
}


void Game::UpdateGame()
{

	while (SDL_GetTicks() - thisFrame >= 16)
	{
		thisFrame = SDL_GetTicks();

		deltaTime = (thisFrame - lastFrame) / 1000;

		if (deltaTime > 0.05)
		{
			deltaTime = 0.05;
		}

		lastFrame = thisFrame;
	}

	SDL_Log("Delta Time: %f", deltaTime);

	//Move Paddle
	if (mMoveDir == -1)
	{
		mPaddlePos.y = mPaddlePos.y + (deltaTime * 200);

		if (mPaddlePos.y >= 708)
		{
			mPaddlePos.y = 708;
		}
	}

	else if (mMoveDir == 1)
	{
		mPaddlePos.y = mPaddlePos.y - (deltaTime * 200);

		if (mPaddlePos.y <= 60)
		{
			mPaddlePos.y = 60;
		}
	}

	//Move Ball
	mBallPos.x = mBallPos.x + (deltaTime * mBallVelocity.x);
	mBallPos.y = mBallPos.y + (deltaTime * mBallVelocity.y);

	if (mBallPos.y >= 747)
	{
		mBallVelocity.y = mBallVelocity.y * -1;
	}

	if (mBallPos.y <= 21)
	{
		mBallPos.y = 22;
		mBallVelocity.y = mBallVelocity.y * -1;
	}


	if ((mBallPos.x >= 1004))
	{
		mBallVelocity.x = mBallVelocity.x * -1;
	}

	if ((mBallPos.x <= 56) && (mBallPos.y <= mPaddlePos.y + 55) && (mBallPos.y >= mPaddlePos.y))
	{
		mBallPos.x = 57;
		mBallVelocity.x = mBallVelocity.x * -1;
	}

	if (mBallPos.x < 0)
	{
		mRunning = false;
	}
}

void Game::GenerateOutput()
{
	SDL_SetRenderDrawColor(mRenderer, 0, 0, 255, 255);
	SDL_RenderClear(mRenderer);
	//Draw gameobjects
	SDL_SetRenderDrawColor(mRenderer, 255, 255, 255, 255);

	SDL_Rect longrect;
	longrect.w = 1024;
	longrect.h = mWallThickness;
	longrect.x = 0;
	longrect.y = 0;
	SDL_RenderFillRect(mRenderer, &longrect);

	longrect.w = 1024;
	longrect.h = mWallThickness;
	longrect.x = 0;
	longrect.y = 768 - mWallThickness;
	SDL_RenderFillRect(mRenderer, &longrect);

	longrect.w = mWallThickness;
	longrect.h = 768;
	longrect.x = 1024 - mWallThickness;
	longrect.y = 0;
	SDL_RenderFillRect(mRenderer, &longrect);

	SDL_Rect paddlerect;
	paddlerect.w = 15;
	paddlerect.h = 100;
	paddlerect.x = mPaddlePos.x;
	paddlerect.y = mPaddlePos.y - 50;
	SDL_RenderFillRect(mRenderer, &paddlerect);

	SDL_Rect ballrect;
	ballrect.w = 10;
	ballrect.h = 10;
	ballrect.x = mBallPos.x - 5;
	ballrect.y = mBallPos.y - 5;
	SDL_RenderFillRect(mRenderer, &ballrect);

	SDL_RenderPresent(mRenderer);
}