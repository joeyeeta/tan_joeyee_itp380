#pragma once
#include "SDL/SDL.h"
#include <string>
using namespace std;

// TODO

class Game
{
public:

	SDL_Window* mWindow;
	SDL_Renderer* mRenderer;
	bool mRunning;
	int mWallThickness;
	int mPaddleHeight;
	SDL_Point mPaddlePos;
	SDL_Point mBallPos;
	int mMoveDir; //-1 down, 0 idle, 1 up 
	SDL_Point mBallVelocity;

	float thisFrame;
	float lastFrame;
	float deltaTime;

	//Constructor
	Game();

	//Functions
	bool Initialize();

	void Shutdown();

	void RunLoop();

	//Game Loop Functions

	void ProcessInput();

	void UpdateGame();

	void GenerateOutput();

};
