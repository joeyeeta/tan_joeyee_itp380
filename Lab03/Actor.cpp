#include "Actor.h"
#include "Game.h"
#include "Component.h"
#include "SpriteComponent.h"
#include "MoveComponent.h"
#include <algorithm>

Actor::Actor(Game* game)
	:mGame(game)
	,mState(EActive)
	,mPosition(Vector2::Zero)
	,mScale(1.0f)
	,mRotation(0.0f)
{
	// TODO
	mMove = nullptr;
	mSprite = nullptr;
	
	game->AddActor(this);
}

Actor::~Actor()
{
	// TODO
	delete mMove;
	delete mSprite;
	
	mGame->RemoveActor(this);
	
}

void Actor::Update(float deltaTime)
{
	// TODO
	if (mState == EActive)
	{
		if (mMove != nullptr)
		{
			mMove->Update(deltaTime);
		}
		//first update all relevant components
		if (mSprite != nullptr)
		{
			mSprite->Update(deltaTime);
		}

		UpdateActor(deltaTime);
	}
}

void Actor::UpdateActor(float deltaTime)
{
}

void Actor::ProcessInput(const Uint8* keyState)
{
	// TODO
	if (mState == EActive)
	{
		if (mMove != nullptr)
		{
			mMove->ProcessInput(keyState);
		}
		// call ProcessInput on all relevant components
		if (mSprite != nullptr)
		{
			mSprite->ProcessInput(keyState);
		}

		ActorInput(keyState);
	}
}

void Actor::ActorInput(const Uint8* keyState)
{
}

SpriteComponent* Actor::GetSprite()
{
	return mSprite;
}

void Actor::SetSprite(SpriteComponent* spritePointer)
{
	mSprite = spritePointer;
}

Vector2 Actor::GetForward()
{
	//Returns forward direction vector
	float x;
	float y;

	x = cos(mRotation);
	y = -(sin(mRotation));

	return Vector2(x, y);
}

//Ship subclass
Ship::Ship(class Game* game)
	:Actor(game)
{
	// TODO
	SpriteComponent* newSprite = new SpriteComponent(this);
	newSprite->SetTexture((game->GetTexture("Assets/Ship.png")));

	MoveComponent* newMove = new MoveComponent(this);

	mSprite = newSprite;
	mMove = newMove;

}

void Ship::ActorInput(const Uint8* keyState)
{
	const Uint8 *state = keyState;
	if (state[SDL_SCANCODE_UP])
	{
		mMove->SetForwardSpeed(100);
		mSprite->SetTexture((mGame->GetTexture("Assets/ShipThrust.png")));
	}

	else if (state[SDL_SCANCODE_DOWN])
	{
		mMove->SetForwardSpeed(-100);
		mSprite->SetTexture((mGame->GetTexture("Assets/ShipThrust.png")));
	}

	else if ((!state[SDL_SCANCODE_UP]) && (!state[SDL_SCANCODE_DOWN]))
	{
		mMove->SetForwardSpeed(0);
		mSprite->SetTexture((mGame->GetTexture("Assets/Ship.png")));
	}

	if (state[SDL_SCANCODE_LEFT])
	{
		mMove->SetAngularSpeed(-5);
	}

	else if (state[SDL_SCANCODE_RIGHT])
	{
		mMove->SetAngularSpeed(5);
	}

	else if ((!state[SDL_SCANCODE_RIGHT]) && (!state[SDL_SCANCODE_LEFT]))
	{
		mMove->SetAngularSpeed(0);
	}
}