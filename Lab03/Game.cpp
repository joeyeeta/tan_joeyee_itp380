//
//  Game.cpp
//  Game-mac
//
//  Created by Sanjay Madhav on 5/31/17.
//  Copyright © 2017 Sanjay Madhav. All rights reserved.
//

#include "Game.h"
#include "Actor.h"
#include "SDL/SDL.h"
#include "SpriteComponent.h"
#include <string>
#include <vector>
#include <unordered_map>
#include <algorithm>
#include <SDL/SDL_image.h>

// TODO

Game::Game()
{
	
}

bool Game::Initialize()
{
	//Initiliaze video and audio
	SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO);

	lastFrame = (SDL_GetTicks() / 1000);
	deltaTime = 0;

	mRunning = true;

	//Create SDL window
	SDL_Window *window;

	window = SDL_CreateWindow(
		"SDL window",
		SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED,
		1024,
		768,
		SDL_WINDOW_OPENGL
	);

	mWindow = window;

	//Create SDL renderer
	SDL_Renderer *renderer;

	renderer = SDL_CreateRenderer(
		window,
		-1,
		SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC
	);

	mRenderer = renderer;

	int initted = IMG_Init(IMG_INIT_PNG);
	SDL_Surface *image;
	image = IMG_Load("Assets/Stars.png");

	mTexture = SDL_CreateTextureFromSurface(renderer, image);

	SDL_FreeSurface(image);

	LoadData();

	return true;
}

void Game::Shutdown()
{
	UnloadData();
	SDL_DestroyRenderer(mRenderer);
	SDL_DestroyWindow(mWindow);
	SDL_Quit();
	IMG_Quit();
}

void Game::RunLoop()
{
	while (mRunning)
	{
		ProcessInput();

		UpdateGame();

		GenerateOutput();
	}
}

void Game::ProcessInput()
{
	

		SDL_Event event;
		while (SDL_PollEvent(&event)) {

			switch (event.type)
			{
			case SDL_QUIT:
				mRunning = false;
				break;
			}
		}

		//Get keyboard state
		const Uint8 *state = SDL_GetKeyboardState(NULL);
		if (state[SDL_SCANCODE_ESCAPE])
		{
			mRunning = false;
		}

		std::vector<Actor*>::iterator it;

		for (it = mActorVector.begin(); it != mActorVector.end(); it++)
		{
			(*it)->ProcessInput(state);
		}

}

void Game::UpdateGame()
{

	while (SDL_GetTicks() - thisFrame >= 16)
	{
		thisFrame = SDL_GetTicks();

		deltaTime = (thisFrame - lastFrame) / 1000;

		if (deltaTime > 0.05)
		{
			deltaTime = 0.05;
		}

		lastFrame = thisFrame;
	}

	SDL_Log("Delta Time: %f", deltaTime);

	std::vector<Actor*> actorVectorCopy;
	actorVectorCopy = mActorVector;

	std::vector<Actor*>::iterator it;

	std::vector<Actor*> deadActorVector;

	for (it = actorVectorCopy.begin(); it != actorVectorCopy.end(); it++)
	{
		(*it)->Update(deltaTime);

		if ((*it)->GetState() == 2)
		{
			deadActorVector.push_back((*it));
		}
	}

	for (it = deadActorVector.begin(); it != deadActorVector.end(); it++)
	{
		deadActorVector.erase(it);
	}

}

void Game::GenerateOutput()
{
	SDL_SetRenderDrawColor(mRenderer, 0, 0, 0, 0);
	SDL_RenderClear(mRenderer);
	//Draw gameobjects

	std::vector<SpriteComponent*>::iterator itr = mSprites.begin();

	while (itr != mSprites.end())
	{
		(*itr)->Draw(mRenderer);
		itr++;
	}

	SDL_RenderPresent(mRenderer);
}

//Game Functions
void Game::AddActor(class Actor* actorPointer)
{
	mActorVector.push_back(actorPointer);
}

void Game::RemoveActor(class Actor* actorPointer)
{
	std::vector<Actor*>::iterator actorItr;

	//Check if there is actor in vector
	actorItr = std::find(mActorVector.begin(), mActorVector.end(), actorPointer);
	if (actorItr != mActorVector.end())
	{
		mActorVector.erase(actorItr);
	}
}

void Game::LoadData()
{
	LoadTexture("Assets/Asteroid.png");
	LoadTexture("Assets/Laser.png");
	LoadTexture("Assets/Ship.png");
	LoadTexture("Assets/ShipThrust.png");
	LoadTexture("Assets/Stars.png");

/*	//Ship
	Actor* shipActor = new Actor(this);
	SpriteComponent* shipSprite = new SpriteComponent(shipActor, 200);
	shipSprite->SetTexture(GetTexture("Assets/Ship.png"));
	shipActor->SetSprite(shipSprite);

	//Laser
	Actor* laserActor = new Actor(this);
	SpriteComponent* laserSprite = new SpriteComponent(laserActor, 150);
	laserSprite->SetTexture(GetTexture("Assets/Laser.png"));
	laserActor->SetSprite(laserSprite);
	laserActor->SetPosition(Vector2(200, 100));

	//ShipThrust
	Actor* ShipThrustActor = new Actor(this);
	SpriteComponent* ShipThrustSprite = new SpriteComponent(ShipThrustActor, 150);
	ShipThrustSprite->SetTexture(GetTexture("Assets/ShipThrust.png"));
	ShipThrustActor->SetSprite(ShipThrustSprite);
	ShipThrustActor->SetPosition(Vector2(200, 200));
	ShipThrustActor->SetScale(0.75);
	ShipThrustActor->SetRotation(Math::PiOver2);
*/

	//Stars
	Actor* starsActor = new Actor(this);
	SpriteComponent* starsSprite = new SpriteComponent(starsActor, 100);
	starsSprite->SetTexture(GetTexture("Assets/Stars.png"));
	starsActor->SetSprite(starsSprite);
	starsActor->SetPosition(Vector2(512, 384));

	//Ship
	Ship* myShip = new Ship(this);
	myShip->SetPosition(Vector2(512, 384));
	
}

void Game::UnloadData()
{

	if (mActorVector.size() != 0)
	{
		mActorVector.pop_back();
	}

	if (mSpriteMap.size() != 0)
	{
		std::unordered_map<std::string, SDL_Texture*>::iterator itr = mSpriteMap.begin();
		while (itr != mSpriteMap.end())
		{
			SDL_DestroyTexture(itr->second);
			itr++;
		}
		mSpriteMap.clear();
	}

}

void Game::LoadTexture(std::string imageName)
{
	int initted = IMG_Init(IMG_INIT_PNG);
	SDL_Surface *image;
	image = IMG_Load(imageName.c_str());
	SDL_Texture *texture = SDL_CreateTextureFromSurface(mRenderer, image);
	SDL_FreeSurface(image);

	mSpriteMap.insert({imageName, texture});


}

SDL_Texture* Game::GetTexture(std::string imageName)
{
	SDL_Texture *texture;
	std::unordered_map<std::string, SDL_Texture*>::iterator itr = mSpriteMap.find(imageName);

	texture = itr->second;

	return texture;
}

void Game::AddSprite(SpriteComponent* spritePointer)
{
	mSprites.push_back(spritePointer);

	std::sort(mSprites.begin(), mSprites.end(),
		[](SpriteComponent* a, SpriteComponent* b) {
		return a->GetDrawOrder() < b->GetDrawOrder();
	});

}

void Game::RemoveSprite(SpriteComponent* spritePointer)
{
	std::vector<SpriteComponent*>::iterator itr;

	itr = std::find(mSprites.begin(), mSprites.end(), spritePointer);
	if (itr != mSprites.end())
	{
		mSprites.erase(itr);
	}
}