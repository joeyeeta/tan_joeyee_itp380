#include "MoveComponent.h"
#include "Actor.h"

MoveComponent::MoveComponent(class Actor* owner)
:Component(owner)
,mAngularSpeed(0.0f)
,mForwardSpeed(0.0f)
{
	
}

void MoveComponent::Update(float deltaTime)
{
	// TODO: Implement in Part 3
	mOwner->SetRotation((mAngularSpeed * deltaTime) + mOwner->GetRotation());

	float x = ((mOwner->GetForward().x) * mForwardSpeed * deltaTime) + mOwner->GetPosition().x;
	float y = ((mOwner->GetForward().y) * mForwardSpeed * deltaTime) + mOwner->GetPosition().y;
	mOwner->SetPosition(Vector2(x, y));
}
