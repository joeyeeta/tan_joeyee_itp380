#include "MoveComponent.h"
#include "BallMove.h"
#include "Actor.h"
#include "CollisionComponent.h"
#include "Game.h"

BallMove::BallMove(class Actor* owner)
	:MoveComponent(owner)
	, mAngularSpeed(0.0f)
	, mForwardSpeed(0.0f)
{
	mVelocity = Vector2(250, -250);
}

void BallMove::Update(float deltaTime)
{
	float x = (mVelocity.x * deltaTime) + mOwner->GetPosition().x;
	float y = (mVelocity.y * deltaTime) + mOwner->GetPosition().y;
	mOwner->SetPosition(Vector2(x, y));

	//Walls
	if (mOwner->GetPosition().x <= 36)
	{
		mOwner->SetPosition(Vector2(37, mOwner->GetPosition().y));
		mVelocity = Vector2(-(mVelocity.x), mVelocity.y);
	}

	if (mOwner->GetPosition().x >= 984)
	{
		mOwner->SetPosition(Vector2(983, mOwner->GetPosition().y));
		mVelocity = Vector2(-(mVelocity.x), mVelocity.y);
	}

	if (mOwner->GetPosition().y <= 36)
	{
		mOwner->SetPosition(Vector2(mOwner->GetPosition().x, 37));
		mVelocity = Vector2(mVelocity.x, -(mVelocity.y));
	}

	if (mOwner->GetPosition().y >= 780)
	{
		mOwner->SetPosition(Vector2((1024 / 2), 650));
		mVelocity = Vector2(250, -250);
	}

	//Paddle
	if (mOwner->GetCol()->Intersect(mOwner->GetGame()->GetPaddle()->GetCol()))
	{
		mOwner->SetPosition(Vector2(mOwner->GetPosition().x, (mOwner->GetPosition().y) - 1));

		//Hits first 1/3
		if (mOwner->GetPosition().x + ((mOwner->GetCol()->GetWidth()) / 2) <= (mOwner->GetGame()->GetPaddle()->GetPosition().x) + ((mOwner->GetGame()->GetPaddle()->GetCol()->GetWidth()) / 3))
		{
			mVelocity = Vector2::Reflect(mVelocity, Vector2(0.3, 1));
		}

		//Hits last 1/3
		else if (mOwner->GetPosition().x - ((mOwner->GetCol()->GetWidth()) / 2) >= (mOwner->GetGame()->GetPaddle()->GetPosition().x) + (((mOwner->GetGame()->GetPaddle()->GetCol()->GetWidth()) / 3)) * 2)
		{
			mVelocity = Vector2::Reflect(mVelocity, Vector2(-0.3, 1));
		}

		//Hits middle
		else
		{
			mVelocity = Vector2::Reflect(mVelocity, Vector2(0, 1));
		}

	}

	//Blocks
	for (int i = 0; i < mOwner->GetGame()->mBlockVector.size(); i++)
	{
		if (mOwner->GetCol()->Intersect(mOwner->GetGame()->mBlockVector[i]->GetCol()))
		{

			//Angle
			float x = mOwner->GetCol()->GetCenter().x - mOwner->GetGame()->mBlockVector[i]->GetCol()->GetCenter().x;
			float y = mOwner->GetCol()->GetCenter().y - mOwner->GetGame()->mBlockVector[i]->GetCol()->GetCenter().y;
			
			//TOPLEFT
			float x1 = mOwner->GetCol()->GetCenter().x - mOwner->GetGame()->mBlockVector[i]->GetCol()->GetMin().x;
			float y1 = mOwner->GetCol()->GetCenter().y - mOwner->GetGame()->mBlockVector[i]->GetCol()->GetMin().y;

			//TOPRIGHT
			float x2 = mOwner->GetCol()->GetCenter().x - mOwner->GetGame()->mBlockVector[i]->GetCol()->GetMax().x;
			float y2 = mOwner->GetCol()->GetCenter().y - mOwner->GetGame()->mBlockVector[i]->GetCol()->GetMin().y;

			//BOTLEFT
			float x3 = mOwner->GetCol()->GetCenter().x - mOwner->GetGame()->mBlockVector[i]->GetCol()->GetMin().x;
			float y3 = mOwner->GetCol()->GetCenter().y - mOwner->GetGame()->mBlockVector[i]->GetCol()->GetMax().y;

			//BOTRIGHT
			float x4 = mOwner->GetCol()->GetCenter().x - mOwner->GetGame()->mBlockVector[i]->GetCol()->GetMax().x;
			float y4 = mOwner->GetCol()->GetCenter().y - mOwner->GetGame()->mBlockVector[i]->GetCol()->GetMax().y;

			if ((Math::Atan2(y, x) >= 0) && (Math::Atan2(y, x) <= (Math::Atan2(y2, x2)))) //Right side
			{
				mVelocity = Vector2(-(mVelocity.x), mVelocity.y);
				
			}

			else if ((Math::Atan2(y, x) <= 0) && (Math::Atan2(y, x) >= (Math::Atan2(y4, x4)))) //Right side
			{
				mVelocity = Vector2(-(mVelocity.x), mVelocity.y);
			}

			else if ((Math::Atan2(y, x) >= (Math::Atan2(y2, x2)) && (Math::Atan2(y, x) <= (Math::Atan2(y1, x1))))) //Top side
			{
				mVelocity = Vector2::Reflect(mVelocity, Vector2(0, 1));
			}

			else if ((Math::Atan2(y, x) <= 180) && (Math::Atan2(y, x) >= (Math::Atan2(y1, x1)))) //Left side
			{
				mVelocity = Vector2(-(mVelocity.x), mVelocity.y);
			}

			else if ((Math::Atan2(y, x) >= -180) && (Math::Atan2(y, x) <= (Math::Atan2(y3, x3)))) //Left side
			{
				mVelocity = Vector2(-(mVelocity.x), mVelocity.y);
			}

			else if ((Math::Atan2(y, x) >= (Math::Atan2(y3, x3)) && (Math::Atan2(y, x) <= (Math::Atan2(y4, x4))))) //Bottom side
			{
				mVelocity = Vector2::Reflect(mVelocity, Vector2(0, 1));
			}

			else
			{
				mVelocity = Vector2::Reflect(mVelocity, Vector2(0, 1));
			}

			mOwner->GetGame()->mBlockVector[i]->SetState(Actor::EDead);

			i = mOwner->GetGame()->mBlockVector.size();
		}
	}

}