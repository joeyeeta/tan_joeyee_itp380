#include "CollisionComponent.h"
#include "Actor.h"

CollisionComponent::CollisionComponent(class Actor* owner)
:Component(owner)
,mWidth(0.0f)
,mHeight(0.0f)
{
	
}

CollisionComponent::~CollisionComponent()
{
	
}

bool CollisionComponent::Intersect(const CollisionComponent* other)
{
	// TODO: Implement
	Vector2 min1 = GetMin();
	Vector2 max1 = GetMax();

	Vector2 min2 = other->GetMin();
	Vector2 max2 = other->GetMax();

	//1 right of 2
	if (max1.x < min2.x)
	{
		return false;
	}

	//1 left of 2
	else if (max2.x < min1.x)
	{
		return false;
	}

	//1 above 2
	else if (min1.y > max2.y)
	{
		return false;
	}

	//2 above 1
	else if (min2.y > max1.y)
	{
		return false;
	}

	else
	{
		return true;
	}
}

Vector2 CollisionComponent::GetMin() const
{
	// TODO: Implement
	int x;
	int y;

	x = mOwner->GetPosition().x - (mWidth * mOwner->GetScale()) / 2;
	y = mOwner->GetPosition().y - (mHeight * mOwner->GetScale()) / 2;
	return Vector2(x, y);
}

Vector2 CollisionComponent::GetMax() const
{
	// TODO: Implement
	int x;
	int y;

	x = mOwner->GetPosition().x + (mWidth * mOwner->GetScale()) / 2;
	y = mOwner->GetPosition().y + (mHeight * mOwner->GetScale()) / 2;
	return Vector2(x, y);
}

const Vector2& CollisionComponent::GetCenter() const
{
	return mOwner->GetPosition();
}

