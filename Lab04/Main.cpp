//
//  Main.cpp
//  Game-mac
//
//  Created by Sanjay Madhav on 5/31/17.
//  Copyright © 2017 Sanjay Madhav. All rights reserved.
//
#include "SDL/SDL.h"
#include "Game.h"
#include <string>
using namespace std;

int main(int argc, char** argv)
{
	// TODO

	//Test game
	Game newGame = Game();

	if (newGame.Initialize() == true)
	{
		newGame.RunLoop();
		newGame.Shutdown();
	}

	return 0;
}
