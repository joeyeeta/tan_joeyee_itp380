#include "MoveComponent.h"
#include "PaddleMove.h"
#include "Actor.h"

PaddleMove::PaddleMove(class Actor* owner)
	:MoveComponent(owner)
	, mAngularSpeed(0.0f)
	, mForwardSpeed(0.0f)
{

}

void PaddleMove::Update(float deltaTime)
{
	MoveComponent::Update(deltaTime);

	if (mOwner->GetPosition().x <= 84)
	{
		mOwner->SetPosition(Vector2(84, mOwner->GetPosition().y));
	}

	if (mOwner->GetPosition().x >= 940)
	{
		mOwner->SetPosition(Vector2(940, mOwner->GetPosition().y));
	}
}