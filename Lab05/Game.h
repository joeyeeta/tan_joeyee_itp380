#pragma once

#include "SDL/SDL.h"
#include <string>
#include <unordered_map>
#include <vector>
#include "Actor.h"
#include "SDL/SDL_mixer.h"

// TODO

class Game
{
public:

	//SDL member variables
	SDL_Window* mWindow;
	SDL_Renderer* mRenderer;
	bool mRunning;

	float thisFrame;
	float lastFrame;
    float deltaTime;

	SDL_Texture *mTexture;

	//Game member variables
	class std::vector<class Actor*> mActorVector;
	class std::unordered_map<std::string, SDL_Texture*> mSpriteMap;
	class std::unordered_map<std::string, Mix_Chunk*> mSoundMap;
	class std::vector<class SpriteComponent*> mSprites;
	class std::vector<class Block*> mBlockVector;
	class Player* mPlayer;
	Vector2 playerPos;

	//Constructor
	Game();

	//Functions
	bool Initialize();

	void Shutdown();

	void RunLoop();

	//Game Loop Functions

	void ProcessInput();

	void UpdateGame();

	void GenerateOutput();

	//Game Functions

	void AddActor(class Actor* actorPointer);

	void RemoveActor(class Actor* actorPointer);

	void LoadData();

	void UnloadData();

	void LoadTexture(std::string imageName);

	SDL_Texture* GetTexture(std::string imageName);

	void AddSprite(SpriteComponent* spritePointer);

	void RemoveSprite(SpriteComponent* spritePointer);

	void LoadSound(const std::string& filename);
	Mix_Chunk* GetSound(const std::string& filename);
};
