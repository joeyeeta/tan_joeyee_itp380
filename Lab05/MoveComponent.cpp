#include "MoveComponent.h"
#include "Actor.h"
#include "SDL/SDL.h"
#include "Game.h"
#include "CollisionComponent.h"
#include "Component.h"
#include "Algorithm"
#include "SDL/SDL_mixer.h"

MoveComponent::MoveComponent(class Actor* owner)
:Component(owner)
,mAngularSpeed(0.0f)
,mForwardSpeed(0.0f)
{
	
}

void MoveComponent::Update(float deltaTime)
{
	// TODO: Implement in Part 3
	mOwner->SetRotation((mAngularSpeed * deltaTime) + mOwner->GetRotation());

	float x = ((mOwner->GetForward().x) * mForwardSpeed * deltaTime) + mOwner->GetPosition().x;
	float y = ((mOwner->GetForward().y) * mForwardSpeed * deltaTime) + mOwner->GetPosition().y;
	mOwner->SetPosition(Vector2(x, y));
}

//PlayerMove subclass
PlayerMove::PlayerMove(class Actor* owner)
	:MoveComponent(owner)
{

}

void PlayerMove::ProcessInput(const Uint8* keyState)
{
	const Uint8 *state = keyState;
	if (state[SDL_SCANCODE_RIGHT])
	{
		SetForwardSpeed(300);
	}

	else if (state[SDL_SCANCODE_LEFT])
	{
		SetForwardSpeed(-300);
	}

	else if (!(state[SDL_SCANCODE_LEFT]) && !(state[SDL_SCANCODE_RIGHT]))
	{
		SetForwardSpeed(0);
	}

	if ((!mSpacePressed) && (state[SDL_SCANCODE_SPACE]))
	{
		if (!mInAir)
		{
			mYSpeed = -800;
		}
		
		Mix_PlayChannel(-1, mOwner->GetGame()->GetSound("Assets/Player/Jump.wav"), 0);
		mInAir = true;
	}

	mSpacePressed = (state[SDL_SCANCODE_SPACE]);

}

void PlayerMove::Update(float deltaTime)
{
	mOwner->SetPosition(Vector2(((mOwner->GetPosition().x) + GetForwardSpeed() * deltaTime), (mOwner->GetPosition().y) + mYSpeed * deltaTime));


	if (mOwner->GetPosition().y > 768)
	{
		mOwner->SetPosition(Vector2((mOwner->GetPosition().x), 768));
		mInAir = false;
	}

	std::vector<Block*>::iterator itr = mOwner->GetGame()->mBlockVector.begin();
	while (itr != mOwner->GetGame()->mBlockVector.end())
	{
		if ((*itr)->mCol->Intersect(mOwner->mCol)) //if intersects
		{
			float dy1 = (mOwner->mCol->GetMax().y) - ((*itr)->mCol->GetMin().y);
			float dy2 = (mOwner->mCol->GetMin().y) - ((*itr)->mCol->GetMax().y);
			float dx1 = ((*itr)->mCol->GetMin().x) - (mOwner->mCol->GetMax().x);
			float dx2 = ((*itr)->mCol->GetMax().x) - (mOwner->mCol->GetMin().x);

			float myFloats[] = { abs(dy1), abs(dy2), abs(dx1), abs(dx2) };

			if ((*(std::min_element(myFloats, myFloats + 4)) == abs(dy1)))
			{
				if (dy1 > 0)
				{
					mOwner->SetPosition(Vector2(mOwner->GetPosition().x, mOwner->GetPosition().y - dy1));
					mYSpeed = 0;
					mInAir = false;
				}
			}

			else if ((*(std::min_element(myFloats, myFloats + 4)) == abs(dy2)))
			{
				if (dy2 < 0)
				{
					mOwner->SetPosition(Vector2(mOwner->GetPosition().x, mOwner->GetPosition().y - dy2));

					if (mYSpeed < 0)
					{
						mYSpeed = 0;
					}
				}
			}

			else if ((*(std::min_element(myFloats, myFloats + 4)) == abs(dx1)))
			{
				if (dx1 < 0)
				{
					mOwner->SetPosition(Vector2(mOwner->GetPosition().x + dx1, mOwner->GetPosition().y));
				}
			}

			else if ((*(std::min_element(myFloats, myFloats + 4)) == abs(dx2)))
			{
				if (dx2 > 0)
				{
					mOwner->SetPosition(Vector2(mOwner->GetPosition().x + dx2, mOwner->GetPosition().y));
				}
			}					
		}

		itr++;
	}

	mYSpeed += 2000 * deltaTime;

}

//BarrelMove subclass
BarrelMove::BarrelMove(class Actor* owner)
	:MoveComponent(owner)
{
	SetForwardSpeed(100);
	SetAngularSpeed(-2*(Math::Pi));
}

void BarrelMove::Update(float deltaTime)
{
	mOwner->SetPosition(Vector2(mOwner->GetPosition().x + deltaTime * GetForwardSpeed(), (mOwner->GetPosition().y) + mYSpeed * deltaTime));
	mOwner->SetRotation(mOwner->GetRotation() + GetAngularSpeed() * deltaTime);

	mYSpeed += 2000 * deltaTime;

	std::vector<Block*>::iterator itr = mOwner->GetGame()->mBlockVector.begin();
	while (itr != mOwner->GetGame()->mBlockVector.end())
	{
		if ((*itr)->mCol->Intersect(mOwner->mCol)) //if intersects
		{
			mYSpeed = 0;
			float dy1 = (mOwner->mCol->GetMax().y) - ((*itr)->mCol->GetMin().y);
			mOwner->SetPosition(Vector2(mOwner->GetPosition().x, mOwner->GetPosition().y - dy1));
		}

		itr++;
	}

	if (mOwner->mCol->Intersect(mOwner->GetGame()->mPlayer->mCol))
	{
		mOwner->GetGame()->mPlayer->SetPosition(mOwner->GetGame()->playerPos);
	}

	if (mOwner->GetPosition().y > 800)
	{
		mOwner->SetState(Actor::EDead);
	}

}