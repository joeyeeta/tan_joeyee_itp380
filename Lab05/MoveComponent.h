#pragma once
#include "Component.h"

class MoveComponent : public Component
{
public:
	MoveComponent(class Actor* owner);

	// Update the move component
	void Update(float deltaTime) override;
	
	// Getters/setters
	float GetAngularSpeed() const { return mAngularSpeed; }
	float GetForwardSpeed() const { return mForwardSpeed; }
	void SetAngularSpeed(float speed) { mAngularSpeed = speed; }
	void SetForwardSpeed(float speed) { mForwardSpeed = speed; }
private:
	// Angular speed (in radians/second)
	float mAngularSpeed;
	// Forward speed (in pixels/second)
	float mForwardSpeed;
};

class PlayerMove : public MoveComponent
{
public:
	PlayerMove(class Actor* owner);
	float mYSpeed = 0;
	bool mSpacePressed = false;
	bool mInAir = false;

	void ProcessInput(const Uint8* keyState) override;
	void Update(float deltaTime) override;

};

class BarrelMove : public MoveComponent
{
public:
	BarrelMove(class Actor* owner);
	float mYSpeed = 0;

	void Update(float deltaTime) override;
};
