#include "Actor.h"
#include "Game.h"
#include "Component.h"
#include "SpriteComponent.h"
#include "CollisionComponent.h"
#include "MoveComponent.h"
#include <SDL/SDL_image.h>
#include <algorithm>

Actor::Actor(Game* game)
	:mGame(game)
	,mState(EActive)
	,mPosition(Vector2::Zero)
	,mScale(1.0f)
	,mRotation(0.0f)
{
	// TODO
	mMove = nullptr;
	mSprite = nullptr;
	mCol = nullptr;
	
	game->AddActor(this);
}

Actor::~Actor()
{
	// TODO
	delete mMove;
	delete mSprite;
	delete mCol;
	
	mGame->RemoveActor(this);
	
}

void Actor::Update(float deltaTime)
{
	// TODO
	if (mState == EActive)
	{
		if (mMove != nullptr)
		{
			mMove->Update(deltaTime);
		}
		//first update all relevant components
		if (mSprite != nullptr)
		{
			mSprite->Update(deltaTime);
		}

		UpdateActor(deltaTime);
	}
}

void Actor::UpdateActor(float deltaTime)
{

}

void Actor::ProcessInput(const Uint8* keyState)
{
	// TODO
	if (mState == EActive)
	{
		if (mMove != nullptr)
		{
			mMove->ProcessInput(keyState);
		}
		// call ProcessInput on all relevant components
		if (mSprite != nullptr)
		{
			mSprite->ProcessInput(keyState);
		}

		if (mCol != nullptr)
		{
			mCol->ProcessInput(keyState);
		}

		ActorInput(keyState);
	}
}

void Actor::ActorInput(const Uint8* keyState)
{
}

SpriteComponent* Actor::GetSprite()
{
	return mSprite;
}

CollisionComponent* Actor::GetCol()
{
	return mCol;
}

void Actor::SetSprite(SpriteComponent* spritePointer)
{
	mSprite = spritePointer;
}

Vector2 Actor::GetForward()
{
	//Returns forward direction vector
	float x;
	float y;

	x = cos(mRotation);
	y = -(sin(mRotation));

	return Vector2(x, y);
}

//Block subclass
Block::Block(class Game* game)
	:Actor(game)
{
	SpriteComponent* newSprite = new SpriteComponent(this);
	newSprite->SetTexture((game->GetTexture("Assets/BlockA.png")));
	CollisionComponent* newCol = new CollisionComponent(this);
	newCol->SetSize(64, 32);
	mCol = newCol;
	mSprite = newSprite;

	game->mBlockVector.push_back(this);
}

Block::~Block()
{
	std::vector<Block*>::iterator itr;
	itr = std::find(mGame->mBlockVector.begin(), mGame->mBlockVector.end(), this);

	if (itr != mGame->mBlockVector.end())
	{
		mGame->mBlockVector.erase(itr);
	}

	mGame->RemoveBlock(this);

}

void Block::ChangeTexture(std::string textureName)
{
	mSprite->SetTexture(mGame->GetTexture(textureName));
}

void Block::UpdateActor(float deltaTime)
{
	if (mPosition.x < mGame->mCameraPos.x - 50)
	{
		SetState(Actor::EDead);
	}
}

//Player subclass
Player::Player(class Game* game)
	:Actor(game)
{
	AnimatedSprite* newSprite = new AnimatedSprite(this);
	//newSprite->SetTexture((game->GetTexture("Assets/Player/Idle.png")));
	CollisionComponent* newCol = new CollisionComponent(this);
	newCol->SetSize(20, 64);
	PlayerMove* newPlayerMove = new PlayerMove(this);
	newSprite->AddImage((game->GetTexture("Assets/Player/Run1.png")));
	newSprite->AddImage((game->GetTexture("Assets/Player/Run2.png")));
	newSprite->AddImage((game->GetTexture("Assets/Player/Run3.png")));
	newSprite->AddImage((game->GetTexture("Assets/Player/Run4.png")));
	newSprite->AddImage((game->GetTexture("Assets/Player/Run5.png")));
	newSprite->AddImage((game->GetTexture("Assets/Player/Run6.png")));
	newSprite->AddImage((game->GetTexture("Assets/Player/Run7.png")));
	newSprite->AddImage((game->GetTexture("Assets/Player/Run8.png")));
	newSprite->AddImage((game->GetTexture("Assets/Player/Run9.png")));
	newSprite->AddImage((game->GetTexture("Assets/Player/Run10.png")));

	mCol = newCol;
	mSprite = newSprite;
	mMove = newPlayerMove;

}

Player::~Player()
{

}

//Barrel subclass
Barrel::Barrel(class Game* game)
	:Actor(game)
{
	SpriteComponent* newSprite = new SpriteComponent(this);
	newSprite->SetTexture((game->GetTexture("Assets/Barrel.png")));
	CollisionComponent* newCol = new CollisionComponent(this);
	newCol->SetSize(32, 32);
	BarrelMove* newBarrelMove = new BarrelMove(this);

	mCol = newCol;
	mSprite = newSprite;
	mMove = newBarrelMove;
}

//BarrelSpawner subclass
BarrelSpawner::BarrelSpawner(class Game* game)
	:Actor(game)
{

}

void BarrelSpawner::UpdateActor(float deltaTime)
{
	mTimer -= deltaTime;
	if (mTimer <= 0)
	{
		Barrel* newBarrel = new Barrel(mGame);
		newBarrel->SetPosition(mPosition);
		mTimer = 3;
	}
}

//Coin subclass
Coin::Coin(class Game* game)
	:Actor(game)
{
	AnimatedSprite* newSprite = new AnimatedSprite(this);
	//newSprite->SetTexture((game->GetTexture("Assets/Coin/coin1.png")));
	CollisionComponent* newCol = new CollisionComponent(this);
	newCol->SetSize(32, 32);
	newSprite->AddImage((game->GetTexture("Assets/Coin/coin1.png")));
	newSprite->AddImage((game->GetTexture("Assets/Coin/coin2.png")));
	newSprite->AddImage((game->GetTexture("Assets/Coin/coin3.png")));
	newSprite->AddImage((game->GetTexture("Assets/Coin/coin4.png")));
	newSprite->AddImage((game->GetTexture("Assets/Coin/coin5.png")));
	newSprite->AddImage((game->GetTexture("Assets/Coin/coin6.png")));
	newSprite->AddImage((game->GetTexture("Assets/Coin/coin7.png")));
	newSprite->AddImage((game->GetTexture("Assets/Coin/coin8.png")));
	newSprite->AddImage((game->GetTexture("Assets/Coin/coin9.png")));
	newSprite->AddImage((game->GetTexture("Assets/Coin/coin10.png")));
	newSprite->AddImage((game->GetTexture("Assets/Coin/coin11.png")));
	newSprite->AddImage((game->GetTexture("Assets/Coin/coin12.png")));
	newSprite->AddImage((game->GetTexture("Assets/Coin/coin13.png")));
	newSprite->AddImage((game->GetTexture("Assets/Coin/coin14.png")));
	newSprite->AddImage((game->GetTexture("Assets/Coin/coin15.png")));
	newSprite->AddImage((game->GetTexture("Assets/Coin/coin16.png")));

	mCol = newCol;
	mSprite = newSprite;
}

void Coin::UpdateActor(float deltaTime)
{
	if (mCol->Intersect(mGame->mPlayer->mCol))
	{
		Mix_PlayChannel(-1, mGame->GetSound("Assets/Coin/coin.wav"), 0);
		SetState(Actor::EDead);
	}

	if (mPosition.x < mGame->mCameraPos.x - 50)
	{
		SetState(Actor::EDead);
	}
}