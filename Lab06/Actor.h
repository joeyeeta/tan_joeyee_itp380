#pragma once
#include <vector>
#include <SDL/SDL_stdinc.h>
#include "Math.h"
class Actor
{
public:
	enum State
	{
		EActive,
		EPaused,
		EDead
	};
	
	Actor(class Game* game);
	virtual ~Actor();

	// Update function called from Game (not overridable)
	void Update(float deltaTime);
	// Any actor-specific update code (overridable)
	virtual void UpdateActor(float deltaTime);
	// ProcessInput function called from Game (not overridable)
	void ProcessInput(const Uint8* keyState);
	// Any actor-specific update code (overridable)
	virtual void ActorInput(const Uint8* keyState);

	// Getters/setters
	const Vector2& GetPosition() const { return mPosition; }
	void SetPosition(const Vector2& pos) { mPosition = pos; }
	float GetScale() const { return mScale; }
	void SetScale(float scale) { mScale = scale; }
	float GetRotation() const { return mRotation; }
	void SetRotation(float rotation) { mRotation = rotation; }
	class MoveComponent* GetMove() const { return mMove; }
	
	State GetState() const { return mState; }
	void SetState(State state) { mState = state; }

	class Game* GetGame() { return mGame; }

	class SpriteComponent* mSprite;
	class CollisionComponent* mCol;

	class SpriteComponent* GetSprite(); //returns mSprite
	class CollisionComponent* GetCol();

	void SetSprite(class SpriteComponent* spritePointer);

	Vector2 GetForward();

	class MoveComponent* mMove;


protected:
	class Game* mGame;
	// Actor's state
	State mState;
	// Transform
	Vector2 mPosition;
	float mScale;
	float mRotation;

};

class Block : public Actor
{
public:
	Block(class Game* game);
	~Block() override;
	void ChangeTexture(std::string textureName);
	void UpdateActor(float deltaTime) override;
};

class Player : public Actor
{
public:
	Player(class Game* game);
	~Player() override;

};

class Barrel : public Actor
{
public:
	Barrel(class Game* game);
};


class BarrelSpawner : public Actor
{
public:
	float mTimer = 3;

	BarrelSpawner(class Game* game);
	void UpdateActor(float deltaTime) override;
};

class Coin : public Actor
{
public:
	Coin(class Game* game);
	void UpdateActor(float deltaTime) override;
};
