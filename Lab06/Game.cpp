//
//  Game.cpp
//  Game-mac
//
//  Created by Sanjay Madhav on 5/31/17.
//  Copyright © 2017 Sanjay Madhav. All rights reserved.
//

#include "Game.h"
#include "Actor.h"
#include "SDL/SDL.h"
#include "SpriteComponent.h"
#include "CollisionComponent.h"
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <unordered_map>
#include <algorithm>
#include <SDL/SDL_image.h>

// TODO

Game::Game()
{
	
}

bool Game::Initialize()
{
	Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048);
	LoadSound("Assets/Player/Jump.wav");
	LoadSound("Assets/Coin/coin.wav");

	//Initiliaze video and audio
	SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO);

	lastFrame = (SDL_GetTicks() / 1000);
	deltaTime = 0;

	mRunning = true;

	//Create SDL window
	SDL_Window *window;

	window = SDL_CreateWindow(
		"SDL window",
		SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED,
		1024,
		768,
		SDL_WINDOW_OPENGL
	);

	mWindow = window;

	//Create SDL renderer
	SDL_Renderer *renderer;

	renderer = SDL_CreateRenderer(
		window,
		-1,
		SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC
	);

	mRenderer = renderer;

	int initted = IMG_Init(IMG_INIT_PNG);
	SDL_Surface *image;
	image = IMG_Load("Assets/Stars.png");

	mTexture = SDL_CreateTextureFromSurface(renderer, image);

	SDL_FreeSurface(image);

	LoadData();

	return true;
}

void Game::Shutdown()
{
	UnloadData();
	Mix_CloseAudio;
	SDL_DestroyRenderer(mRenderer);
	SDL_DestroyWindow(mWindow);
	SDL_Quit();
	IMG_Quit();
}

void Game::RunLoop()
{
	while (mRunning)
	{
		ProcessInput();

		UpdateGame();

		GenerateOutput();
	}
}

void Game::ProcessInput()
{
		SDL_Event event;
		while (SDL_PollEvent(&event)) {

			switch (event.type)
			{
			case SDL_QUIT:
				mRunning = false;
				break;
			}
		}

		//Get keyboard state
		const Uint8 *state = SDL_GetKeyboardState(NULL);
		if (state[SDL_SCANCODE_ESCAPE])
		{
			mRunning = false;
		}

		std::vector<Actor*>::iterator it;

		for (it = mActorVector.begin(); it != mActorVector.end(); it++)
		{
			(*it)->ProcessInput(state);
		}

}

void Game::UpdateGame()
{

	while (SDL_GetTicks() - thisFrame >= 16)
	{
		thisFrame = SDL_GetTicks();

		deltaTime = (thisFrame - lastFrame) / 1000;

		if (deltaTime > 0.05)
		{
			deltaTime = 0.05;
		}

		lastFrame = thisFrame;
	}

	std::vector<Actor*> actorVectorCopy;
	actorVectorCopy = mActorVector;

	std::vector<Actor*>::iterator it;

	std::vector<Actor*> deadActorVector;

	for (it = actorVectorCopy.begin(); it != actorVectorCopy.end(); it++)
	{
		(*it)->Update(deltaTime);

		if ((*it)->GetState() == 2)
		{
			deadActorVector.push_back((*it));
		}
	}

	for (it = deadActorVector.begin(); it != deadActorVector.end(); it++)
	{
		delete(*it);
	}

	if (((mPlayer->GetPosition().x) > 2560 + (mLevels * 2560)) && ((mPlayer->GetPosition().x) < 2570 + (mLevels * 2560)))
	{
		LoadNextLevel();
	}

}

void Game::GenerateOutput()
{
	SDL_SetRenderDrawColor(mRenderer, 0, 0, 255, 0);
	SDL_RenderClear(mRenderer);
	//Draw gameobjects

	std::vector<SpriteComponent*>::iterator itr = mSprites.begin();

	while (itr != mSprites.end())
	{
		(*itr)->Draw(mRenderer);
		itr++;
	}

	SDL_RenderPresent(mRenderer);
}

//Game Functions
void Game::AddActor(class Actor* actorPointer)
{
	mActorVector.push_back(actorPointer);
}

void Game::RemoveActor(class Actor* actorPointer)
{
	std::vector<Actor*>::iterator actorItr;

	//Check if there is actor in vector
	actorItr = std::find(mActorVector.begin(), mActorVector.end(), actorPointer);
	if (actorItr != mActorVector.end())
	{
		mActorVector.erase(actorItr);
	}
}

void Game::RemoveBlock(class Block* blockPointer)
{
	std::vector<Block*>::iterator blockItr;

	//Check if there is actor in vector
	blockItr = std::find(mBlockVector.begin(), mBlockVector.end(), blockPointer);
	if (blockItr != mBlockVector.end())
	{
		mBlockVector.erase(blockItr);
	}
}

void Game::LoadData()
{
	LoadTexture("Assets/Background/Sky_0.png");
	LoadTexture("Assets/Background/Sky_1.png");
	LoadTexture("Assets/Background/Sky_2.png");
	LoadTexture("Assets/Background/Mid_0.png");
	LoadTexture("Assets/Background/Mid_1.png");
	LoadTexture("Assets/Background/Mid_2.png");
	LoadTexture("Assets/Background/Fore_0.png");
	LoadTexture("Assets/Background/Fore_1.png");
	LoadTexture("Assets/Background/Fore_2.png");

	LoadTexture("Assets/BlockA.png");
	LoadTexture("Assets/BlockB.png");
	LoadTexture("Assets/BlockC.png");
	LoadTexture("Assets/BlockD.png");
	LoadTexture("Assets/BlockE.png");
	LoadTexture("Assets/BlockF.png");

	LoadTexture("Assets/Player/Idle.png");
	LoadTexture("Assets/Player/Run1.png");
	LoadTexture("Assets/Player/Run2.png");
	LoadTexture("Assets/Player/Run3.png");
	LoadTexture("Assets/Player/Run4.png");
	LoadTexture("Assets/Player/Run5.png");
	LoadTexture("Assets/Player/Run6.png");
	LoadTexture("Assets/Player/Run7.png");
	LoadTexture("Assets/Player/Run8.png");
	LoadTexture("Assets/Player/Run9.png");
	LoadTexture("Assets/Player/Run10.png");
	LoadTexture("Assets/Barrel.png");
	LoadTexture("Assets/Coin/coin1.png");
	LoadTexture("Assets/Coin/coin2.png");
	LoadTexture("Assets/Coin/coin3.png");
	LoadTexture("Assets/Coin/coin4.png");
	LoadTexture("Assets/Coin/coin5.png");
	LoadTexture("Assets/Coin/coin6.png");
	LoadTexture("Assets/Coin/coin7.png");
	LoadTexture("Assets/Coin/coin8.png");
	LoadTexture("Assets/Coin/coin9.png");
	LoadTexture("Assets/Coin/coin10.png");
	LoadTexture("Assets/Coin/coin11.png");
	LoadTexture("Assets/Coin/coin12.png");
	LoadTexture("Assets/Coin/coin13.png");
	LoadTexture("Assets/Coin/coin14.png");
	LoadTexture("Assets/Coin/coin15.png");
	LoadTexture("Assets/Coin/coin16.png");

	Actor* backgroundActor = new Actor(this);
	Background* newBackground = new Background(backgroundActor, 0);
	newBackground->AddImage(GetTexture("Assets/Background/Sky_0.png"));
	newBackground->AddImage(GetTexture("Assets/Background/Sky_1.png"));
	newBackground->AddImage(GetTexture("Assets/Background/Sky_2.png"));
	newBackground->mParallax = 0.25;
	backgroundActor->SetSprite(newBackground);
	backgroundActor->SetPosition(Vector2(-512, 0));

	Actor* backgroundActor2 = new Actor(this);
	Background* newBackground2 = new Background(backgroundActor2, 1);
	newBackground2->AddImage(GetTexture("Assets/Background/Mid_0.png"));
	newBackground2->AddImage(GetTexture("Assets/Background/Mid_1.png"));
	newBackground2->AddImage(GetTexture("Assets/Background/Mid_2.png"));
	newBackground2->mParallax = 0.5;
	backgroundActor2->SetSprite(newBackground2);
	backgroundActor2->SetPosition(Vector2(-512, 0));

	Actor* backgroundActor3 = new Actor(this);
	Background* newBackground3 = new Background(backgroundActor3, 2);
	newBackground3->AddImage(GetTexture("Assets/Background/Fore_0.png"));
	newBackground3->AddImage(GetTexture("Assets/Background/Fore_1.png"));
	newBackground3->AddImage(GetTexture("Assets/Background/Fore_2.png"));
	newBackground3->mParallax = 0.75;
	backgroundActor3->SetSprite(newBackground3);
	backgroundActor3->SetPosition(Vector2(-512, 0));

	LoadNextLevel();
}


void Game::UnloadData()
{

	if (mActorVector.size() != 0)
	{
		mActorVector.pop_back();
	}

	if (mSpriteMap.size() != 0)
	{
		std::unordered_map<std::string, SDL_Texture*>::iterator itr = mSpriteMap.begin();
		while (itr != mSpriteMap.end())
		{
			SDL_DestroyTexture(itr->second);
			itr++;
		}
		mSpriteMap.clear();
	}

	Mix_FreeChunk(mSoundMap.find("Assets/Player/Jump.wav")->second);

}

void Game::LoadTexture(std::string imageName)
{
	int initted = IMG_Init(IMG_INIT_PNG);
	SDL_Surface *image;
	image = IMG_Load(imageName.c_str());
	SDL_Texture *texture = SDL_CreateTextureFromSurface(mRenderer, image);
	SDL_FreeSurface(image);

	mSpriteMap.insert({imageName, texture});


}

SDL_Texture* Game::GetTexture(std::string imageName)
{
	SDL_Texture *texture;
	std::unordered_map<std::string, SDL_Texture*>::iterator itr = mSpriteMap.find(imageName);

	texture = itr->second;

	return texture;
}

void Game::AddSprite(SpriteComponent* spritePointer)
{
	mSprites.push_back(spritePointer);

	std::sort(mSprites.begin(), mSprites.end(),
		[](SpriteComponent* a, SpriteComponent* b) {
		return a->GetDrawOrder() < b->GetDrawOrder();
	});

}

void Game::RemoveSprite(SpriteComponent* spritePointer)
{
	std::vector<SpriteComponent*>::iterator itr;

	itr = std::find(mSprites.begin(), mSprites.end(), spritePointer);
	if (itr != mSprites.end())
	{
		mSprites.erase(itr);
	}
}

void Game::LoadSound(const std::string& filename)
{
	Mix_Chunk *jumpsound;
	jumpsound = Mix_LoadWAV(filename.c_str());

	mSoundMap.insert({ filename, jumpsound });
}

Mix_Chunk* Game::GetSound(const std::string& filename){	return mSoundMap.find(filename)->second;}
const Vector2& Game::GetCameraPos() const
{
	return mCameraPos;
}

void Game::SetCameraPos(const Vector2& cameraPos)
{
	mCameraPos = cameraPos;
}

void Game::LoadNextLevel()
{
	//Load Level
	std::string levelName = "Assets/Level" + std::to_string(mLevelNumber) + ".txt";
	std::ifstream levelFile(levelName);
	mLevelNumber++;
	mLevels++;
	if (mLevelNumber >= 4)
	{
		mLevelNumber = 0;
	}

	std::string line;
	int lineNumber = 1;

	std::unordered_map<char, std::string> blockType;
	blockType['A'] = "Assets/BlockA.png";
	blockType['B'] = "Assets/BlockB.png";
	blockType['C'] = "Assets/BlockC.png";
	blockType['D'] = "Assets/BlockD.png";
	blockType['E'] = "Assets/BlockE.png";
	blockType['F'] = "Assets/BlockF.png";

	if (levelFile.is_open())
	{
		while (getline(levelFile, line))
		{
			for (int x = 0; x < line.length(); x++)
			{
				char currentChar = line.at(x);

				if ((currentChar != '.'))
				{
					if (currentChar == 'P')
					{
						if (mPlayer == nullptr)
						{
							Player* newPlayer = new Player(this);
							newPlayer->SetPosition(Vector2(32 + (x * 64), -16 + (lineNumber * 32)));
							playerPos = Vector2(32 + (x * 64), -16 + (lineNumber * 32));
							mPlayer = newPlayer;
						}
					}

					else if (currentChar == 'O')
					{
						BarrelSpawner* newBarrelSpawner = new BarrelSpawner(this);
						newBarrelSpawner->SetPosition(Vector2(32 + (x * 64), -16 + (lineNumber * 32)));
					}

					else if (currentChar == '*')
					{
						Coin* newCoin = new Coin(this);
						newCoin->SetPosition(Vector2(32 + (x * 64) + (56 * 64 * mLevels), -32 + (lineNumber * 32)));
					}

					else
					{
						Block* newBlock = new Block(this);
						newBlock->ChangeTexture(blockType.at(currentChar));
						newBlock->SetPosition(Vector2(32 + (x * 64) + (56 * 64 * mLevels), 16 + ((lineNumber - 1) * 32)));
					}
				}
			}

			lineNumber++;
		}
	}
}
