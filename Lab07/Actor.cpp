#include "Actor.h"
#include "Game.h"
#include "Component.h"
#include "SpriteComponent.h"
#include "CollisionComponent.h"
#include "MoveComponent.h"
#include <SDL/SDL_image.h>
#include <algorithm>

Actor::Actor(Game* game)
	:mGame(game)
	,mState(EActive)
	,mPosition(Vector2::Zero)
	,mScale(1.0f)
	,mRotation(0.0f)
{
	// TODO
	mMove = nullptr;
	mSprite = nullptr;
	mCol = nullptr;
	
	game->AddActor(this);
}

Actor::~Actor()
{
	// TODO
	delete mMove;
	delete mSprite;
	delete mCol;
	
	mGame->RemoveActor(this);
	
}

void Actor::Update(float deltaTime)
{
	// TODO
	if (mState == EActive)
	{
		if (mMove != nullptr)
		{
			mMove->Update(deltaTime);
		}
		//first update all relevant components
		if (mSprite != nullptr)
		{
			mSprite->Update(deltaTime);
		}

		UpdateActor(deltaTime);
	}
}

void Actor::UpdateActor(float deltaTime)
{

}

void Actor::ProcessInput(const Uint8* keyState)
{
	// TODO
	if (mState == EActive)
	{
		if (mMove != nullptr)
		{
			mMove->ProcessInput(keyState);
		}
		// call ProcessInput on all relevant components
		if (mSprite != nullptr)
		{
			mSprite->ProcessInput(keyState);
		}

		if (mCol != nullptr)
		{
			mCol->ProcessInput(keyState);
		}

		ActorInput(keyState);
	}
}

void Actor::ActorInput(const Uint8* keyState)
{
}

SpriteComponent* Actor::GetSprite()
{
	return mSprite;
}

CollisionComponent* Actor::GetCol()
{
	return mCol;
}

void Actor::SetSprite(SpriteComponent* spritePointer)
{
	mSprite = spritePointer;
}

Vector2 Actor::GetForward()
{
	//Returns forward direction vector
	float x;
	float y;

	x = cos(mRotation);
	y = -(sin(mRotation));

	return Vector2(x, y);
}
