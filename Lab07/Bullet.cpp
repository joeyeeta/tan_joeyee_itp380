#include "Actor.h"
#include "Game.h"
#include "Component.h"
#include "SpriteComponent.h"
#include "CollisionComponent.h"
#include "MoveComponent.h"
#include "Bullet.h"
#include "SDL/SDL.h"
#include "PlaneMove.h"
#include "Plane.h"
#include <SDL/SDL_image.h>
#include <algorithm>

Bullet::Bullet(class Game* game)
	:Actor(game)
{
	SpriteComponent* newSprite = new SpriteComponent(this, 200);
	newSprite->SetTexture(mGame->GetTexture("Assets/Bullet.png"));
	mSprite = newSprite;

	CollisionComponent* newCol = new CollisionComponent(this);
	newCol->SetSize(10, 10);
	mCol = newCol;

	MoveComponent* newMove = new MoveComponent(this);
	newMove->SetForwardSpeed(500);
	mMove = newMove;
}

void Bullet::UpdateActor(float deltaTime)
{
	mTimer += deltaTime;

	if (mTimer > 2)
	{
		SetState(State::EDead);
	}
	else
	{
		std::vector<Plane*>::iterator itr = mGame->mPlanes.begin();
		while (itr != mGame->mPlanes.end())
		{
			if (mCol->Intersect((*itr)->mCol))
			{
				(*itr)->SetState(State::EDead);
				SetState(State::EDead);
			}
			itr++;
		}
	}
}