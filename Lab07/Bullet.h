#pragma once
#include <vector>
#include <SDL/SDL_stdinc.h>
#include "Actor.h"
#include "Math.h"
#include "SpriteComponent.h"

class Bullet : public Actor
{
public:
	float mTimer;

	Bullet(class Game* game);
	void UpdateActor(float deltaTime) override;
};