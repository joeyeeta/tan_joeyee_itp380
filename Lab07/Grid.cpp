#include "Grid.h"
#include "Tile.h"
#include "Tower.h"
#include "Plane.h"
#include "Game.h"
#include <SDL/SDL.h>
#include <algorithm>
#include <cmath>

Grid::Grid(class Game* game)
	:Actor(game)
	,mSelectedTile(nullptr)
{
	// 7 rows, 16 columns
	mTiles.resize(NumRows);
	for (size_t i = 0; i < mTiles.size(); i++)
	{
		mTiles[i].resize(NumCols);
	}
	
	// Create tiles
	for (size_t i = 0; i < NumRows; i++)
	{
		for (size_t j = 0; j < NumCols; j++)
		{
			mTiles[i][j] = new Tile(GetGame());
			mTiles[i][j]->SetPosition(Vector2(TileSize/2.0f + j * TileSize, StartY + i * TileSize));
		}
	}
	
	// Set start/end tiles
	GetStartTile()->SetTileState(Tile::EStart);
	GetEndTile()->SetTileState(Tile::EBase);
	
	// Set up adjacency lists
	for (size_t i = 0; i < NumRows; i++)
	{
		for (size_t j = 0; j < NumCols; j++)
		{
			if (i > 0)
			{
				mTiles[i][j]->mAdjacent.push_back(mTiles[i-1][j]);
			}
			if (i < NumRows - 1)
			{
				mTiles[i][j]->mAdjacent.push_back(mTiles[i+1][j]);
			}
			if (j > 0)
			{
				mTiles[i][j]->mAdjacent.push_back(mTiles[i][j-1]);
			}
			if (j < NumCols - 1)
			{
				mTiles[i][j]->mAdjacent.push_back(mTiles[i][j+1]);
			}
		}
	}

	TryFindPath();
	UpdatePathTiles();
}

void Grid::SelectTile(size_t row, size_t col)
{
	// Make sure it's a valid selection
	Tile::TileState tstate = mTiles[row][col]->GetTileState();
	if (tstate != Tile::EStart && tstate != Tile::EBase)
	{
		// Deselect previous one
		if (mSelectedTile)
		{
			mSelectedTile->ToggleSelect();
		}
		mSelectedTile = mTiles[row][col];
		mSelectedTile->ToggleSelect();
	}
}

Tile* Grid::GetStartTile()
{
	return mTiles[3][0];
}

Tile* Grid::GetEndTile()
{
	return mTiles[3][15];
}

void Grid::ActorInput(const Uint8 * keyState)
{
	// Process mouse click to select a tile
	int x, y;
	Uint32 buttons = SDL_GetMouseState(&x, &y);
	if (SDL_BUTTON(buttons) & SDL_BUTTON_LEFT)
	{
		// Calculate the x/y indices into the grid
		y -= static_cast<int>(StartY - TileSize / 2);
		if (y >= 0)
		{
			x /= static_cast<int>(TileSize);
			y /= static_cast<int>(TileSize);
			if (x >= 0 && x < static_cast<int>(NumCols) && y >= 0 && y < static_cast<int>(NumRows))
			{
				SelectTile(y, x);
			}
		}
	}

	const Uint8 *state = keyState;
	if ((mSelectedTile != nullptr) && (!mSpacePressed) && (state[SDL_SCANCODE_SPACE]))
	{
		mNeedToBuild = true;
	}
	mSpacePressed = (state[SDL_SCANCODE_SPACE]);
}

void Grid::UpdateActor(float deltaTime)
{
	if (mNeedToBuild)
	{
		BuildTower(mSelectedTile);
		mNeedToBuild = false;
	}

	mTimer += deltaTime;
	if (mTimer >= 1)
	{
		class Plane* newPlane = new Plane(mGame);
		newPlane->SetPosition(GetStartTile()->mPosition);
		mTimer = 0;
	}
}

bool Grid::TryFindPath()
{
	mOpenSet.clear();
	//Reset all tiles to not in closed set, and g = 0
	for (int y = 0; y < mTiles.size(); y++)
	{
		for (int z = 0; z < mTiles[y].size(); z++)
		{
			mTiles[y][z]->mInClosedSet = false;
			mTiles[y][z]->g = 0;
		}
	}

	Tile* currentTile = GetEndTile();
	currentTile->mInClosedSet = true;

	currentTile->h = abs(currentTile->GetPosition().x - GetStartTile()->GetPosition().x) + abs(currentTile->GetPosition().y - GetStartTile()->GetPosition().y);
	currentTile->g = TileSize;
	currentTile->f = currentTile->h + currentTile->g;

	do
	{
		for (int i = 0; i < currentTile->mAdjacent.size(); i++)
		{
			//check to see if in open set
			bool inOpenSet = false;

			for (int n = 0; n < mOpenSet.size(); n++)
			{
				if (mOpenSet[n] == currentTile->mAdjacent[i])
				{
					inOpenSet = true;
				}
			}

			if (currentTile->mAdjacent[i]->mInClosedSet) //if in closed set
			{
				continue;
			}

			else if (inOpenSet) //if in open set // check for adoption
			{
				//compute new g with currentnode as parent
				float newg = currentTile->g + TileSize;

				if (newg < currentTile->mAdjacent[i]->g)
				{
					currentTile->mAdjacent[i]->mParent = currentTile;
					currentTile->mAdjacent[i]->g = newg;
					currentTile->mAdjacent[i]->f = currentTile->mAdjacent[i]->g + currentTile->mAdjacent[i]->h;
				}

			}

			else if (!(currentTile->mBlocked))
			{
				currentTile->mAdjacent[i]->mParent = currentTile;
				//compute n.h
				currentTile->mAdjacent[i]->h = abs(currentTile->mAdjacent[i]->GetPosition().x - GetStartTile()->GetPosition().x) + abs(currentTile->mAdjacent[i]->GetPosition().y - GetStartTile()->GetPosition().y);
				//compute n.g
				currentTile->mAdjacent[i]->g = currentTile->mAdjacent[i]->mParent->g + TileSize;
				currentTile->mAdjacent[i]->f = currentTile->mAdjacent[i]->g + currentTile->mAdjacent[i]->h;

				mOpenSet.push_back(currentTile->mAdjacent[i]);
			}
		}

		if (mOpenSet.size() <= 0)
		{
			break;
		}

		//find node in openset with lowest f, & make currentnode that node
		auto itr = mOpenSet.begin();
		if (itr == mOpenSet.end())
		{
			continue;
		}
		currentTile = *itr;
		
		for (; itr != mOpenSet.end(); itr++)
		{
			Tile* n = *itr;
			if (n->f < currentTile->f)
			{
				currentTile = n;
			}
		}

		//remove currentnode from openset
		std::vector<Tile*>::iterator tileItr;
		tileItr = std::find(mOpenSet.begin(), mOpenSet.end(), currentTile);
		if (tileItr != mOpenSet.end())
		{
			mOpenSet.erase(tileItr);
		}
		currentTile->mInClosedSet = true;
	}

	while (currentTile != GetStartTile());

	if (currentTile == GetStartTile())
	{
		return true;
	}

	return false;
}

void Grid::UpdatePathTiles()
{
	for (int y = 0; y < mTiles.size(); y++)
	{
		for (int z = 0; z < mTiles[y].size(); z++)
		{
			if ((mTiles[y][z] != GetStartTile()) && ((mTiles[y][z] != GetEndTile())))
			{
				mTiles[y][z]->SetTileState(Tile::EDefault);
			}
		}
	}

	Tile* previousTile = GetStartTile();

	while(previousTile->mParent != GetEndTile())
	{
		previousTile->mParent->SetTileState(Tile::EPath);
		previousTile = previousTile->mParent;
	}
}

void Grid::BuildTower(Tile* currentTile)
{
	if (currentTile->mBlocked)
	{
		//dont do anything
	}
	else
	{
		currentTile->mBlocked = true;
		TryFindPath();
		UpdatePathTiles();

		if (TryFindPath() == false)
		{
			currentTile->mBlocked = false;
			TryFindPath();
		}

		else
		{
			Tower* newTower = new Tower(mGame);
			newTower->SetPosition(currentTile->GetPosition());
		}
	}

}

