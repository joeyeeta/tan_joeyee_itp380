#include "Actor.h"
#include "Game.h"
#include "Component.h"
#include "SpriteComponent.h"
#include "CollisionComponent.h"
#include "MoveComponent.h"
#include "Plane.h"
#include "SDL/SDL.h"
#include "PlaneMove.h"
#include <SDL/SDL_image.h>
#include <algorithm>

Plane::Plane(class Game* game)
	:Actor(game)
{
	SetPosition(mGame->mGrid->GetStartTile()->GetPosition());

	SpriteComponent* newSprite = new SpriteComponent(this, 200);
	newSprite->SetTexture(mGame->GetTexture("Assets/Airplane.png"));
	mSprite = newSprite;

	CollisionComponent* newCol = new CollisionComponent(this);
	newCol->SetSize(64, 64);
	mCol = newCol;

	PlaneMove* newMove = new PlaneMove(this);
	mMove = newMove;

	mGame->mPlanes.push_back(this);
}

Plane::~Plane()
{
	std::vector<Plane*>::iterator itr;

	itr = std::find(mGame->mPlanes.begin(), mGame->mPlanes.end(), this);
	if (itr != mGame->mPlanes.end())
	{
		mGame->mPlanes.erase(itr);
	}
}