#pragma once
#include <vector>
#include <SDL/SDL_stdinc.h>
#include "Actor.h"
#include "Math.h"
#include "SpriteComponent.h"

class Plane : public Actor
{
public:
	Plane(class Game* game);
	~Plane();
};