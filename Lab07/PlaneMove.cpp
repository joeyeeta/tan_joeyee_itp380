#include "MoveComponent.h"
#include "Actor.h"
#include "SDL/SDL.h"
#include "Game.h"
#include "CollisionComponent.h"
#include "Component.h"
#include "Algorithm"
#include "SDL/SDL_mixer.h"
#include "SpriteComponent.h"
#include "PlaneMove.h"
#include "Tile.h"

PlaneMove::PlaneMove(Actor* owner)
	:MoveComponent(owner)
{
	SetForwardSpeed(200);
	SetNextTile(mOwner->GetGame()->mGrid->GetStartTile()->GetParent());
}

void PlaneMove::Update(float deltaTime)
{
	MoveComponent::Update(deltaTime);

	if ((mNextTile != nullptr) && ((mOwner->GetPosition() - mNextTile->GetPosition()).x * (mOwner->GetPosition() - mNextTile->GetPosition()).x + (mOwner->GetPosition() - mNextTile->GetPosition()).y * (mOwner->GetPosition() - mNextTile->GetPosition()).y < 2 * 2))
	{
		SetNextTile(mNextTile->GetParent());
	}

	if (mOwner->GetPosition().x > 1500)
	{
		mOwner->SetState(Actor::EDead);
	}
}

void PlaneMove::SetNextTile(const Tile* tile)
{
	mNextTile = tile;
	if (mNextTile == nullptr)
	{
		mOwner->SetRotation(0);
	}
	else
	{
		Vector2 newVector = tile->GetPosition() - mOwner->GetPosition();
		float angle = atan2(-(newVector.y), newVector.x);
		mOwner->SetRotation(angle);
	}
}