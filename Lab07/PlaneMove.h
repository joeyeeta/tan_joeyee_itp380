#include "MoveComponent.h"
#include "Actor.h"
#include "SDL/SDL.h"
#include "Game.h"
#include "CollisionComponent.h"
#include "Component.h"
#include "Algorithm"
#include "SDL/SDL_mixer.h"
#include "Tile.h"
#include "SpriteComponent.h"

class PlaneMove : public MoveComponent
{
public:
	const Tile* mNextTile;

	PlaneMove(Actor* owner);
	void Update(float deltaTime) override;
	void SetNextTile(const Tile* tile);
};