#include "SpriteComponent.h"
#include "Actor.h"
#include "Game.h"
#include "Component.h"
#include "MoveComponent.h"
#include <vector>

SpriteComponent::SpriteComponent(Actor* owner, int drawOrder)
	:Component(owner)
	,mTexture(nullptr)
	,mDrawOrder(drawOrder)
	,mTexWidth(0)
	,mTexHeight(0)
{
	mOwner->GetGame()->AddSprite(this);

}

SpriteComponent::~SpriteComponent()
{
	mOwner->GetGame()->RemoveSprite(this);
}

void SpriteComponent::Draw(SDL_Renderer* renderer)
{
	if (mTexture)
	{
		SDL_Rect r;
		r.w = static_cast<int>(mTexWidth * mOwner->GetScale());
		r.h = static_cast<int>(mTexHeight * mOwner->GetScale());
		// Center the rectangle around the position of the owner
		r.x = static_cast<int>((mOwner->GetPosition().x - r.w / 2) - (mOwner->GetGame()->mCameraPos.x));
		r.y = static_cast<int>((mOwner->GetPosition().y - r.h / 2) - (mOwner->GetGame()->mCameraPos.y));

		// Draw (have to convert angle from radians to degrees, and clockwise to counter)
		SDL_RenderCopyEx(renderer,
			mTexture,
			nullptr,
			&r,
			-Math::ToDegrees(mOwner->GetRotation()),
			nullptr,
			SDL_FLIP_NONE);
	}
}

void SpriteComponent::SetTexture(SDL_Texture* texture)
{
	mTexture = texture;
	// Set width/height
	SDL_QueryTexture(texture, nullptr, nullptr, &mTexWidth, &mTexHeight);
}

Background::Background(Actor* owner, int drawOrder)
	:SpriteComponent(owner, drawOrder)
{
	mBackgroundN = 0;
}

Background::~Background()
{

}

void Background::AddImage(SDL_Texture* image)
{
	mTextureVector.push_back(image);
}

void Background::Draw(SDL_Renderer* renderer)
{
	int xPos = mOwner->GetPosition().x - mOwner->GetGame()->mCameraPos.x * mParallax;
	int backgroundNumber = 0;

	while (xPos < 1024)
	{
		SDL_Rect r;
		r.w = static_cast<int>(1024);
		r.h = static_cast<int>(768);
		// Center the rectangle around the position of the owner
		r.x = static_cast<int>(xPos);
		r.y = static_cast<int>((mOwner->GetPosition().y) - (mOwner->GetGame()->mCameraPos.y));

		// Draw (have to convert angle from radians to degrees, and clockwise to counter)
		SDL_RenderCopyEx(renderer,
			mTextureVector[backgroundNumber],
			nullptr,
			&r,
			-Math::ToDegrees(mOwner->GetRotation()),
			nullptr,
			SDL_FLIP_NONE);

		xPos += 1024;
		backgroundNumber++;
		backgroundNumber %= mTextureVector.size();
	}
}

//AnimatedSprite subclass
AnimatedSprite::AnimatedSprite(Actor* owner, int drawOrder)
	:SpriteComponent(owner, drawOrder)
{

}

AnimatedSprite::~AnimatedSprite()
{

}

void AnimatedSprite::AddImage(SDL_Texture* image)
{
	mImages.push_back(image);
}

void AnimatedSprite::Update(float deltaTime)
{
	mAnimTimer = mAnimTimer + mAnimSpeed * deltaTime;
	int currentFrame = (int)mAnimTimer;
	if (currentFrame >= mImages.size())
	{
		currentFrame = 0;
		mAnimTimer = 0;
	}
	SetTexture(mImages[currentFrame]);
}
