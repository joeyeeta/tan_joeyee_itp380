#pragma once
#include "Component.h"
#include "SDL/SDL.h"
#include <vector>
class SpriteComponent : public Component
{
public:
	// (Lower draw order corresponds with further back)
	SpriteComponent(class Actor* owner, int drawOrder = 100);
	~SpriteComponent();

	// Draw this sprite
	virtual void Draw(SDL_Renderer* renderer);
	// Set the texture to draw for this psirte
	virtual void SetTexture(SDL_Texture* texture);

	// Get the draw order for this sprite
	int GetDrawOrder() const { return mDrawOrder; }
	// Get the width/height of the texture
	int GetTexHeight() const { return mTexHeight; }
	int GetTexWidth() const { return mTexWidth; }

protected:
	// Texture to draw
	SDL_Texture* mTexture;
	// Draw order
	int mDrawOrder;
	// Width/height
	int mTexWidth;
	int mTexHeight;

};

class Background : public SpriteComponent
{
public:
	class std::vector<SDL_Texture*> mTextureVector;
	int mBackgroundN = 0;
	int vectorNumber = 0; //max 2
	float mParallax = 1;

	Background(class Actor* owner, int drawOrder = 100);
	~Background();
	void AddImage(SDL_Texture* image);
	void Draw(SDL_Renderer* renderer) override;
};

class AnimatedSprite : public SpriteComponent
{
public:
	std::vector<SDL_Texture*> mImages;
	float mAnimTimer = 0;
	float mAnimSpeed = 15;

	AnimatedSprite(class Actor* owner, int drawOrder = 100);
	~AnimatedSprite();
	void AddImage(SDL_Texture* image);
	void Update(float deltaTime) override;
};
