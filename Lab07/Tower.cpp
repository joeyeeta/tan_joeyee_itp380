#include "Actor.h"
#include "Game.h"
#include "Component.h"
#include "SpriteComponent.h"
#include "CollisionComponent.h"
#include "MoveComponent.h"
#include "Tower.h"
#include "Plane.h"
#include "SDL/SDL.h"
#include "Bullet.h"
#include "SDL\SDL.h"
#include <SDL/SDL_image.h>
#include <algorithm>

Tower::Tower(class Game* game)
	:Actor(game)
{
	SpriteComponent* newSprite = new SpriteComponent(this, 200);
	newSprite->SetTexture(mGame->GetTexture("Assets/Tower.png"));
	mSprite = newSprite;
}

void Tower::UpdateActor(float deltaTime)
{
	mAttackTimer += deltaTime;

	if (mAttackTimer >= 2)
	{
		float distance = (this->GetPosition() - FindClosestPlane()->GetPosition()).x * (this->GetPosition() - FindClosestPlane()->GetPosition()).x + (this->GetPosition() - FindClosestPlane()->GetPosition()).y * (this->GetPosition() - FindClosestPlane()->GetPosition()).y;
		if (distance <= 100 * 100)
		{
			Vector2 newVector = FindClosestPlane()->GetPosition() - GetPosition();
			float angle = atan2(-(newVector.y), newVector.x);
			SetRotation(angle);

			Bullet* newBullet = new Bullet(mGame);
			newBullet->SetPosition(GetPosition());
			newBullet->SetRotation(GetRotation());

		}
		mAttackTimer = 0;
	}
}

Plane* Tower::FindClosestPlane()
{
	Plane* closestPlane = nullptr;
	for (int i = 0; i < mGame->mPlanes.size(); i++)
	{
		if (i == 0)
		{
			closestPlane = mGame->mPlanes[0];
		}

		else
		{
			float newDistance = (this->GetPosition() - mGame->mPlanes[i]->GetPosition()).x * (this->GetPosition() - mGame->mPlanes[i]->GetPosition()).x + (this->GetPosition() - mGame->mPlanes[i]->GetPosition()).y * (this->GetPosition() - mGame->mPlanes[i]->GetPosition()).y;
			float closestDistance = (this->GetPosition() - closestPlane->GetPosition()).x * (this->GetPosition() - closestPlane->GetPosition()).x + (this->GetPosition() - closestPlane->GetPosition()).y * (this->GetPosition() - closestPlane->GetPosition()).y;
			if (newDistance < closestDistance)
			{
				closestPlane = mGame->mPlanes[i];
			}
		}
	}

	return closestPlane;
}