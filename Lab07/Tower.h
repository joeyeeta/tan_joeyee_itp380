#pragma once
#include <vector>
#include <SDL/SDL_stdinc.h>
#include "Actor.h"
#include "Math.h"
#include "Plane.h"
#include "SpriteComponent.h"

class Tower : public Actor
{
public:
	float mAttackTimer = 0;
	SpriteComponent* mSprite;

	Tower(class Game* game);
	void UpdateActor(float deltaTime) override;
	Plane* FindClosestPlane();
};