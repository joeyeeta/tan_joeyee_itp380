#include "MeshComponent.h"
#include "MoveComponent.h"
#include "Component.h"
#include "CollisionComponent.h"
#include "Actor.h"
#include "Bullet.h"
#include <vector>
#include "Block.h"
#include "Tank.h"
#include "Renderer.h"
#include "Game.h"

Bullet::Bullet(Game* game)
	:Actor(game)
{
	mMove = new MoveComponent(this);
	mMesh = new MeshComponent(this);
	mMesh->SetMesh(game->GetRenderer()->GetMesh("Assets/Sphere.gpmesh"));
	mCol = new CollisionComponent(this);
	mCol->SetSize(10, 10, 10);

	SetScale(0.5);
	mMove->SetForwardSpeed(400);

}

void Bullet::UpdateActor(float deltaTime)
{
	for (int i = 0; i < mGame->mBlockVector.size(); i++)
	{
		if (mCol->Intersect(mGame->mBlockVector[i]->mCol))
		{
			this->SetState(Actor::EDead);
			i = mGame->mBlockVector.size();
		}

		else if (mCol->Intersect(mGame->mTank1->mCol))
		{
			if (mGame->mTank1 != mTank)
			{
				this->SetState(Actor::EDead);
				mGame->mTank1->Respawn();
			}
		}

		else if (mCol->Intersect(mGame->mTank2->mCol))
		{
			if (mGame->mTank2 != mTank)
			{
				this->SetState(Actor::EDead);
				mGame->mTank2->Respawn();
			}
		}
	}
}
