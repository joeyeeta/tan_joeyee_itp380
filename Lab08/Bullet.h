#pragma once
#include "MoveComponent.h"
#include "MeshComponent.h"
#include "CollisionComponent.h"
#include "Tank.h"
#include "Actor.h"

class Bullet : public Actor
{
public:
	Tank* mTank;

	Bullet(Game* game);
	void SetTank(Tank* tank) { mTank = tank; }
	void UpdateActor(float deltaTime) override;
};

