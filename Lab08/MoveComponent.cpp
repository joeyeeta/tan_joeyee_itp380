#include "MoveComponent.h"
#include "Actor.h"
#include "SDL/SDL.h"
#include "Game.h"
#include "CollisionComponent.h"
#include "Component.h"
#include "Algorithm"
#include "SDL/SDL_mixer.h"

MoveComponent::MoveComponent(class Actor* owner)
:Component(owner)
,mAngularSpeed(0.0f)
,mForwardSpeed(0.0f)
{
	
}

void MoveComponent::Update(float deltaTime)
{
	// TODO: Implement in Part 3
	mOwner->SetRotation((mAngularSpeed * deltaTime) + mOwner->GetRotation());

	float x = ((mOwner->GetForward().x) * mForwardSpeed * deltaTime) + mOwner->GetPosition().x;
	float y = ((mOwner->GetForward().y) * mForwardSpeed * deltaTime) + mOwner->GetPosition().y;
	float z = ((mOwner->GetForward().z) * mForwardSpeed * deltaTime) + mOwner->GetPosition().z;
	mOwner->SetPosition(Vector3(x, y, z));
}
