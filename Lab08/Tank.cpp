#include <vector>
#include <SDL/SDL_stdinc.h>
#include "Math.h"
#include "Tank.h"
#include "Actor.h"
#include "MeshComponent.h"
#include "Game.h"
#include "Renderer.h"
#include "Bullet.h"
#include "SDL\SDL.h"
#include "Turret.h"
#include "CollisionComponent.h"
#include "TankMove.h"
#include "MoveComponent.h"

Tank::Tank(Game* game)
	:Actor(game)
{
	mMesh = new MeshComponent(this);
	mMesh->SetMesh(game->GetRenderer()->GetMesh("Assets/TankBase.gpmesh"));
	mMove = new TankMove(this);
	mTurret = new Turret(game);
	mCol = new CollisionComponent(this);
	mCol->SetSize(30, 30, 30);
	//mPos = mGame->mTankPos1;
}

void Tank::UpdateActor(float deltatime)
{
	mTurret->SetPosition(GetPosition());
}

void Tank::ActorInput(const Uint8 * keyState)
{
	if ((keyState[mFireKey]) && !(mFirePressed))
	{
		Fire();
		mFirePressed = true;
	}

	if (!(keyState[mFireKey]))
	{
		mFirePressed = false;
	}
}

void Tank::SetPlayerTwo()
{
	TankMove* tankMove = static_cast<TankMove*>(mMove);
	tankMove->SetPlayerTwo();
	mTurret->SetPlayerTwo();
	mMesh->SetTextureIndex(1);
	//mPos = mGame->mTankPos2;
	mFireKey = SDL_SCANCODE_RSHIFT;
}

void Tank::Fire()
{
	Bullet* newBullet = new Bullet(mGame);
	newBullet->SetPosition(GetPosition());
	newBullet->SetRotation(mTurret->GetRotation());
	newBullet->SetTank(this);
}

void Tank::Respawn()
{
	SetPosition(mPos);
	SetRotation(0);
	mTurret->SetRotation(0);
}