#pragma once
#include <vector>
#include <SDL/SDL_stdinc.h>
#include "Math.h"
#include "Actor.h"
#include "Turret.h"

class Tank : public Actor
{
public:
	Turret* mTurret;
	Vector3 mPos;
	SDL_Scancode mFireKey = SDL_SCANCODE_LSHIFT;
	bool mFirePressed = false;

	Tank(class Game* game);
	void UpdateActor(float deltatime) override;
	void ActorInput(const Uint8* keyState) override;
	void SetPlayerTwo();
	void Fire();
	void Respawn();
};