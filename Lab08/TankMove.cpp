#include "MoveComponent.h"
#include "Actor.h"
#include "SDL/SDL.h"
#include "Game.h"
#include "Block.h"
#include "CollisionComponent.h"
#include "Component.h"
#include "Algorithm"
#include "SDL/SDL_mixer.h"
#include "Tank.h"
#include "TankMove.h"

TankMove::TankMove(Actor* owner)
	:MoveComponent(owner)
{

}

void TankMove::Update(float deltaTime)
{
	MoveComponent::Update(deltaTime);

	std::vector<Block*>::iterator itr = mOwner->GetGame()->mBlockVector.begin();
	while (itr != mOwner->GetGame()->mBlockVector.end())
	{
		if ((*itr)->mCol->Intersect(mOwner->mCol)) //if intersects
		{
			float dy1 = (mOwner->mCol->GetMax().y) - ((*itr)->mCol->GetMin().y);
			float dy2 = (mOwner->mCol->GetMin().y) - ((*itr)->mCol->GetMax().y);
			float dx1 = ((*itr)->mCol->GetMin().x) - (mOwner->mCol->GetMax().x);
			float dx2 = ((*itr)->mCol->GetMax().x) - (mOwner->mCol->GetMin().x);

			float myFloats[] = { abs(dy1), abs(dy2), abs(dx1), abs(dx2) };

			if ((*(std::min_element(myFloats, myFloats + 4)) == abs(dy1)))
			{
				if (dy1 > 0)
				{
					mOwner->SetPosition(Vector3(mOwner->GetPosition().x, mOwner->GetPosition().y - dy1, mOwner->GetPosition().z));
				}
			}

			else if ((*(std::min_element(myFloats, myFloats + 4)) == abs(dy2)))
			{
				if (dy2 < 0)
				{
					mOwner->SetPosition(Vector3(mOwner->GetPosition().x, mOwner->GetPosition().y - dy2, mOwner->GetPosition().z));
				}
			}

			else if ((*(std::min_element(myFloats, myFloats + 4)) == abs(dx1)))
			{
				if (dx1 < 0)
				{
					mOwner->SetPosition(Vector3(mOwner->GetPosition().x + dx1, mOwner->GetPosition().y, mOwner->GetPosition().z));
				}
			}

			else if ((*(std::min_element(myFloats, myFloats + 4)) == abs(dx2)))
			{
				if (dx2 > 0)
				{
					mOwner->SetPosition(Vector3(mOwner->GetPosition().x + dx2, mOwner->GetPosition().y, mOwner->GetPosition().z));
				}
			}
		}

		itr++;
	}
}

void TankMove::ProcessInput(const Uint8* keyState)
{
	if (keyState[mForwardKey])
	{
		SetForwardSpeed(250);
	}

	else if (keyState[mBackKey])
	{
		SetForwardSpeed(-250);
	}

	else if ((!keyState[mBackKey]) && (!keyState[mForwardKey]))
	{
		SetForwardSpeed(0);
	}

	if (keyState[mLeftKey])
	{
		SetAngularSpeed(Math::TwoPi);
	}

	else if (keyState[mRightKey])
	{
		SetAngularSpeed(-(Math::TwoPi));
	}

	else if ((!keyState[mLeftKey]) && (!keyState[mRightKey]))
	{
		SetAngularSpeed(0);
	}
}

void TankMove::SetPlayerTwo()
{
	mForwardKey = SDL_SCANCODE_O;
	mBackKey = SDL_SCANCODE_L;
	mLeftKey = SDL_SCANCODE_K;
	mRightKey = SDL_SCANCODE_SEMICOLON;
}