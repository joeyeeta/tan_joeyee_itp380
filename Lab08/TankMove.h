#pragma once
#include "MoveComponent.h"

class TankMove : public MoveComponent
{
public:
	SDL_Scancode mForwardKey = SDL_SCANCODE_W;
	SDL_Scancode mBackKey = SDL_SCANCODE_S;
	SDL_Scancode mLeftKey = SDL_SCANCODE_A;
	SDL_Scancode mRightKey = SDL_SCANCODE_D;

	TankMove(class Actor* owner);
	void Update(float deltaTime) override;
	void ProcessInput(const Uint8* keyState) override;
	void SetPlayerTwo();
};