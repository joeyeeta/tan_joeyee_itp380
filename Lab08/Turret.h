#pragma once
#include <vector>
#include <SDL/SDL_stdinc.h>
#include "Math.h"
#include "SDL\SDL.h"
#include "MoveComponent.h"
#include "Actor.h"

class Turret : public Actor
{
public:
	SDL_Scancode mLeftKey = SDL_SCANCODE_E;
	SDL_Scancode mRightKey = SDL_SCANCODE_Q;

	Turret(class Game* game);
	void ActorInput(const Uint8* keyState) override;
	void SetPlayerTwo();
};