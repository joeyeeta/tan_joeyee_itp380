#include "Actor.h"
#include "Game.h"
#include "Component.h"
#include "Renderer.h"
#include "CollisionComponent.h"
#include "MeshComponent.h"
#include "MoveComponent.h"
#include <SDL/SDL_image.h>
#include <algorithm>
#include "Block.h"

Block::Block(Game* game)
	:Actor(game)
{
	mMesh = new MeshComponent(this);
	mMesh->SetMesh(game->GetRenderer()->GetMesh("Assets/Cube.gpmesh"));
	mScale = 64;
	mCol = new CollisionComponent(this);
	mCol->SetSize(1, 1, 1);
	game->mBlockVector.push_back(this);
}

Block::~Block()
{
	std::vector<Block*>::iterator itr = mGame->mBlockVector.begin();
	while (itr != mGame->mBlockVector.end())
	{
		if (*(itr) == this)
		{
			mGame->mBlockVector.erase(itr);
		}
	}
}