#include "CameraComponent.h"
#include "Component.h"
#include "Actor.h"
#include "Renderer.h"
#include "Math.h"
#include "Game.h"

CameraComponent::CameraComponent(Actor * owner)
	:Component(owner)
{

}

void CameraComponent::Update(float deltaTime)
{
	Matrix4 pitchMatrix = Matrix4::CreateRotationY(mPitchAngle);
	Matrix4 yawMatrix = Matrix4::CreateRotationZ(mOwner->GetRotation());
	Matrix4 rotateMatrix = pitchMatrix * yawMatrix;
	Vector3 forwardVector = Vector3::Transform(Vector3(1, 0, 0), rotateMatrix);

	Matrix4 LookAtMatrix = Matrix4::CreateLookAt(mOwner->GetPosition(), forwardVector * 2 + mOwner->GetPosition(), Vector3(0, 0, 1));

	mOwner->GetGame()->GetRenderer()->SetViewMatrix(LookAtMatrix);
	mPitchAngle += mPitchSpeed * deltaTime;

	if (mPitchAngle < -Math::Pi / 4)
	{
		mPitchAngle = -Math::Pi / 4;
	}

	if (mPitchAngle > Math::Pi / 4)
	{
		mPitchAngle = Math::Pi / 4;
	}
}
