#include "Player.h"
#include "Actor.h"
#include "Game.h"
#include "CollisionComponent.h"
#include "CameraComponent.h"
#include "PlayerMove.h"

Player::Player(Game * game)
	:Actor(game)
{
	mMove = new PlayerMove(this);
	mCol = new CollisionComponent(this);
	mCol->SetSize(50, 175, 50);
	mCamera = new CameraComponent(this);
}
