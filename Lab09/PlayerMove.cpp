#include "PlayerMove.h"
#include "Math.h"
#include "CameraComponent.h"
#include "CollisionComponent.h"
#include "Actor.h"
#include "Game.h"
#include "Block.h"
#include <vector>
#include <algorithm>

PlayerMove::PlayerMove(Actor * owner)
	:MoveComponent(owner)
{
	ChangeState(Falling);
}

void PlayerMove::Update(float deltaTime)
{
	if (mCurrentState == OnGround)
	{
		UpdateOnGround(deltaTime);
	}

	else if (mCurrentState == Jump)
	{
		UpdateJump(deltaTime);
	}

	else if (mCurrentState == Falling)
	{
		UpdateFalling(deltaTime);
	}
}

void PlayerMove::ProcessInput(const Uint8 * keyState)
{
	int x, y;
	SDL_GetRelativeMouseState(&x, &y);

	float newX = (float)x;
	float newY = (float)y;
	SetAngularSpeed(Math::Pi * 10 * (newX / 500));
	mOwner->mCamera->SetPitchSpeed(Math::Pi * 10 * (newY / 500));

	if (keyState[SDL_SCANCODE_W])
	{
		SetForwardSpeed(350);
	}

	else if (keyState[SDL_SCANCODE_S])
	{
		SetForwardSpeed(-350);
	}

	else if (!(keyState[SDL_SCANCODE_W]) && !(keyState[SDL_SCANCODE_S]))
	{
		SetForwardSpeed(0);
	}

	else if ((keyState[SDL_SCANCODE_W]) && (keyState[SDL_SCANCODE_S]))
	{
		SetForwardSpeed(0);
	}

	if (keyState[SDL_SCANCODE_D])
	{
		SetStrafeSpeed(350);
	}

	else if (keyState[SDL_SCANCODE_A])
	{
		SetStrafeSpeed(-350);
	}

	else if (!(keyState[SDL_SCANCODE_D]) && !(keyState[SDL_SCANCODE_A]))
	{
		SetStrafeSpeed(0);
	}

	else if ((keyState[SDL_SCANCODE_D]) && (keyState[SDL_SCANCODE_A]))
	{
		SetStrafeSpeed(0);
	}

	if (keyState[SDL_SCANCODE_SPACE])
	{
		if ((mSpacedPressed) && (mCurrentState == OnGround))
		{
			ChangeState(Jump);
			mZSpeed = JumpSpeed;
		}

		mSpacedPressed = true;
	}

	else
	{
		mSpacedPressed = false;
	}
}

void PlayerMove::ChangeState(MoveState newState)
{
	mCurrentState = newState;
}

void PlayerMove::UpdateOnGround(float deltaTime)
{
	MoveComponent::Update(deltaTime);
	bool falling = true;

	for (int i = 0; i < mOwner->GetGame()->mBlockVector.size(); i++)
	{
		FixCollision(mOwner->mCol, mOwner->GetGame()->mBlockVector[i]->mCol);
		if (FixCollision(mOwner->mCol, mOwner->GetGame()->mBlockVector[i]->mCol) == Top)
		{
			falling = false;
		}
	}

	if (falling)
	{
		ChangeState(Falling);
	}
}

void PlayerMove::UpdateJump(float deltaTime)
{
	MoveComponent::Update(deltaTime);
	mZSpeed += Gravity * deltaTime;
	mOwner->SetPosition(Vector3(mOwner->GetPosition().x, mOwner->GetPosition().y, mOwner->GetPosition().z + mZSpeed * deltaTime));

	for (int i = 0; i < mOwner->GetGame()->mBlockVector.size(); i++)
	{
		FixCollision(mOwner->mCol, mOwner->GetGame()->mBlockVector[i]->mCol);
		if (FixCollision(mOwner->mCol, mOwner->GetGame()->mBlockVector[i]->mCol) == Bottom)
		{
			mZSpeed = 0;
			ChangeState(Falling);
		}
		if (mZSpeed <= 0)
		{
			ChangeState(Falling);
		}
	}
}

void PlayerMove::UpdateFalling(float deltaTime)
{
	MoveComponent::Update(deltaTime);
	mZSpeed += deltaTime * Gravity;
	mOwner->SetPosition(Vector3(mOwner->GetPosition().x, mOwner->GetPosition().y, mOwner->GetPosition().z + mZSpeed * deltaTime));
	for (int i = 0; i < mOwner->GetGame()->mBlockVector.size(); i++)
	{
		FixCollision(mOwner->mCol, mOwner->GetGame()->mBlockVector[i]->mCol);
		if (FixCollision(mOwner->mCol, mOwner->GetGame()->mBlockVector[i]->mCol) == Top)
		{
			mZSpeed = 0;
			ChangeState(OnGround);
		}
	}
}

PlayerMove::CollSide PlayerMove::FixCollision(CollisionComponent * self, CollisionComponent * block)
{
	Vector3 pos = mOwner->GetPosition();

	if (self->Intersect(block))
	{
		// Get player min/max and block min/max
		Vector3 playerMin = self->GetMin();
		Vector3 playerMax = self->GetMax();
		Vector3 blockMin = block->GetMin();
		Vector3 blockMax = block->GetMax();

		// Figure out which side we are closest to
		float dx1 = blockMin.x - playerMax.x;
		float dx2 = blockMax.x - playerMin.x;
		float dy1 = blockMin.y - playerMax.y;
		float dy2 = blockMax.y - playerMin.y;
		float dz1 = blockMin.z - playerMax.z;
		float dz2 = blockMax.z - playerMin.z;

		float dx = 0.0f;
		if (Math::Abs(dx1) < Math::Abs(dx2))
		{
			dx = dx1;
		}
		else
		{
			dx = dx2;
		}

		float dy = 0.0f;
		if (Math::Abs(dy1) < Math::Abs(dy2))
		{
			dy = dy1;
		}
		else
		{
			dy = dy2;
		}

		float dz = 0.0f;
		if (Math::Abs(dz1) < Math::Abs(dz2))
		{
			dz = dz1;
		}
		else
		{
			dz = dz2;
		}

		// Are we closer to top or bottom?
		if (Math::Abs(dy) < Math::Abs(dx))
		{
			if (Math::Abs(dy) < Math::Abs(dz))
			{
				pos.y += dy;
				mOwner->SetPosition(pos);
				return Side;
			}

			else
			{
				pos.z += dz;
				if (dz == dz1)
				{
					mOwner->SetPosition(pos);
					return Bottom;
				}

				else
				{
					mOwner->SetPosition(pos);
					return Top;
				}
			}
		}
		else
		{
			if (Math::Abs(dx) < Math::Abs(dz))
			{
				pos.x += dx;
				mOwner->SetPosition(pos);
				return Side;
			}

			else
			{
				pos.z += dz;
				if (dz == dz1)
				{
					mOwner->SetPosition(pos);
					return Bottom;
				}

				else
				{
					mOwner->SetPosition(pos);
					return Top;
				}
			}
		}
	}

	else
	{
		return None;
	}
}


