#pragma once
#include "MoveComponent.h"
#include "SDL\SDL.h"

class PlayerMove : public MoveComponent
{
public:
	enum MoveState
	{
		OnGround,
		Jump,
		Falling
	};

	MoveState mCurrentState;
	float mZSpeed = 0;
	const float Gravity = -980;
	const float JumpSpeed = 500;
	bool mSpacedPressed = false;

	PlayerMove(class Actor* owner);
	void Update(float deltaTime) override;
	void ProcessInput(const Uint8* keyState) override;
	void ChangeState(MoveState newState);

protected:
	enum CollSide
	{
		None,
		Top,
		Bottom,
		Side
	};
	void UpdateOnGround(float deltaTime);
	void UpdateJump(float deltaTime);
	void UpdateFalling(float deltaTime);
	CollSide FixCollision(class CollisionComponent* self, class CollisionComponent* block);
};