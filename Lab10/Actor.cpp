#include "Actor.h"
#include "Game.h"
#include "Component.h"
#include "CollisionComponent.h"
#include "MeshComponent.h"
#include "MoveComponent.h"
#include "CameraComponent.h"
#include <SDL/SDL_image.h>
#include <algorithm>

Actor::Actor(Game* game)
	:mGame(game)
	,mState(EActive)
	,mPosition(Vector3::Zero)
	,mScale(1.0f)
	,mRotation(0.0f)
{
	// TODO
	mMove = nullptr;
	mCol = nullptr;
	
	game->AddActor(this);
	mMesh = nullptr;
}

Actor::~Actor()
{
	// TODO
	delete mMove;
	delete mCol;
	delete mMesh;
	delete mCamera;
	
	mGame->RemoveActor(this);
	
}

void Actor::Update(float deltaTime)
{
	// TODO
	if (mState == EActive)
	{
		if (mMove != nullptr)
		{
			mMove->Update(deltaTime);
		}

		if (mCamera != nullptr)
		{
			mCamera->Update(deltaTime);
		}

		if (mMesh != nullptr)
		{
			mMesh->Update(deltaTime);
		}

		UpdateActor(deltaTime);
		
	}

	//Set mworldtransform
	Matrix4 scaleMatrix = Matrix4::CreateScale(mScale);
	Matrix4 rotationMatrix = Matrix4::CreateRotationZ(GetRotation());
	Matrix4 positionMatrix = Matrix4::CreateTranslation(GetPosition());
	mWorldTransform = scaleMatrix * rotationMatrix * positionMatrix;
}

void Actor::UpdateActor(float deltaTime)
{

}

void Actor::ProcessInput(const Uint8* keyState)
{
	// TODO
	if (mState == EActive)
	{
		if (mMove != nullptr)
		{
			mMove->ProcessInput(keyState);
		}

		if (mCol != nullptr)
		{
			mCol->ProcessInput(keyState);
		}

		if (mCamera != nullptr)
		{
			mCamera->ProcessInput(keyState);
		}

		if (mMesh != nullptr)
		{
			mMesh->ProcessInput(keyState);
		}
		ActorInput(keyState);
	}
}

void Actor::ActorInput(const Uint8* keyState)
{
}

CollisionComponent* Actor::GetCol()
{
	return mCol;
}

Vector3 Actor::GetForward()
{
	return Vector3(Math::Cos(mRotation), Math::Sin(mRotation), 0.0f);
}

Vector3 Actor::GetRight()
{
	return Vector3(Math::Cos(mRotation + Math::PiOver2), Math::Sin(mRotation + Math::PiOver2), 0.0f);
}

const Matrix4& Actor::GetWorldTransform() const 
{ 
	return mWorldTransform; 
}
