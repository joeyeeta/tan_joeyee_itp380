#pragma once
#include "MoveComponent.h"
#include <vector>
#include "SDL\SDL.h"
#include "Math.h"

class PlayerMove : public MoveComponent
{
public:
	enum MoveState
	{
		OnGround,
		Jump,
		Falling,
		WallClimb,
		WallRun
	};

	MoveState mCurrentState;
	Vector3 mGravity = Vector3(0, 0, -980);
	Vector3 mJumpForce = Vector3(0, 0, 35000);
	float mMass = 1;
	Vector3 mVelocity;
	Vector3 mAcceleration;
	Vector3 mPendingForces;
	bool mSpacedPressed = false;
	Vector3 mWallClimbForce = Vector3(0, 0, 1800);
	float mWallClimbTimer;
	Vector3 mWallRunForce = Vector3(0, 0, 1200);
	float mWallRunTimer = 0;
	

	PlayerMove(class Actor* owner);
	void FixXYVelocity();
	void PhysicsUpdate(float deltaTime);
	void AddForce(const Vector3& force);
	void Update(float deltaTime) override;
	void ProcessInput(const Uint8* keyState) override;
	void ChangeState(MoveState newState);

protected:
	enum CollSide
	{
		None,
		Top,
		Bottom,
		SideX1,
		SideX2,
		SideY1,
		SideY2
	};
	void UpdateOnGround(float deltaTime);
	void UpdateJump(float deltaTime);
	void UpdateFalling(float deltaTime);
	void UpdateWallClimb(float deltaTime);
	void UpdateWallRun(float deltaTime);
	bool CanWallClimb(CollSide side);
	bool CanWallRun(CollSide side);
	CollSide FixCollision(class CollisionComponent* self, class CollisionComponent* block);
};