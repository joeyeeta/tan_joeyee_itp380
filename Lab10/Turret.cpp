#include <vector>
#include <SDL/SDL_stdinc.h>
#include "Math.h"
#include "Turret.h"
#include "MeshComponent.h"
#include "Mesh.h"
#include "Game.h"
#include "Renderer.h"
#include "MoveComponent.h"
#include "Actor.h"
#include "SDL/SDL.h"
#include "Game.h"
#include "CollisionComponent.h"
#include "Component.h"
#include "Algorithm"
#include "SDL/SDL_mixer.h"

Turret::Turret(Game* game)
	:Actor(game)
{
	mMesh = new MeshComponent(this);
	mMesh->SetMesh(game->GetRenderer()->GetMesh("Assets/TankTurret.gpmesh"));
	mMove = new MoveComponent(this);
	mScale = 1.8f;
}

void Turret::ActorInput(const Uint8* keyState)
{
	if (keyState[mRightKey])
	{
		mMove->SetAngularSpeed(Math::TwoPi);
	}

	else if (keyState[mLeftKey])
	{
		mMove->SetAngularSpeed(-(Math::TwoPi));
	}

	else if (!(keyState[mRightKey]) && !(keyState[mLeftKey]))
	{
		mMove->SetAngularSpeed(0);
	}
}

void Turret::SetPlayerTwo()
{
	mLeftKey = SDL_SCANCODE_P;
	mRightKey = SDL_SCANCODE_I;
	mMesh->SetTextureIndex(1);
}