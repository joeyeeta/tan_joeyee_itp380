#pragma once
#include <vector>
#include <SDL/SDL_stdinc.h>
#include "Math.h"
class Actor
{
public:
	enum State
	{
		EActive,
		EPaused,
		EDead
	};
	
	Actor(class Game* game);
	virtual ~Actor();

	// Update function called from Game (not overridable)
	void Update(float deltaTime);
	// Any actor-specific update code (overridable)
	virtual void UpdateActor(float deltaTime);
	// ProcessInput function called from Game (not overridable)
	void ProcessInput(const Uint8* keyState);
	// Any actor-specific update code (overridable)
	virtual void ActorInput(const Uint8* keyState);

	// Getters/setters
	const Vector3& GetPosition() const { return mPosition; }
	void SetPosition(const Vector3& pos) { mPosition = pos; }
	float GetScale() const { return mScale; }
	void SetScale(float scale) { mScale = scale; }
	float GetRotation() const { return mRotation; }
	void SetRotation(float rotation) { mRotation = rotation; }
	class MoveComponent* GetMove() const { return mMove; }
	class MeshComponent* GetMesh() { return mMesh; }
	
	State GetState() const { return mState; }
	void SetState(State state) { mState = state; }
	class CameraComponent* GetCamera() { return mCamera; }

	class Game* GetGame() { return mGame; }

	class CollisionComponent* mCol;
	class CollisionComponent* GetCol();

	class MoveComponent* mMove;

	Matrix4 mWorldTransform;
	const Matrix4& GetWorldTransform() const;

	Vector3 GetForward();
	Vector3 GetRight();

	class MeshComponent* mMesh;

	class CameraComponent* mCamera = nullptr;

	Quaternion mQuat;
	void SetQuat(Quaternion quat) { mQuat = quat; }
	Quaternion GetQuat() { return mQuat; }

protected:
	class Game* mGame;
	// Actor's state
	State mState;
	// Transform
	Vector3 mPosition;
	float mScale;
	float mRotation;

};

