#include "Player.h"
#include "Actor.h"
#include "Game.h"
#include "Renderer.h"
#include "MeshComponent.h"
#include "CollisionComponent.h"
#include "Arrow.h"
#include "Checkpoint.h"

Arrow::Arrow(Game * game)
	:Actor(game)
{
	mMesh = new MeshComponent(this);
	mMesh->SetMesh(mGame->GetRenderer()->GetMesh("Assets/Arrow.gpmesh"));
	SetScale(0.15);
}

void Arrow::UpdateActor(float deltaTime)
{
	if (mGame->mCheckpointQueue.front() == NULL)
	{
		mQuat = Quaternion::Identity;
	}

	else
	{
		Vector3 pointingVector = mGame->mCheckpointQueue.front()->GetPosition() - mGame->mPlayer->GetPosition();
		pointingVector.Normalize();

		float dotProduct = Vector3::Dot(Vector3(1, 0, 0), pointingVector);
		float angle = acos(dotProduct);
		Vector3 axis = Vector3::Normalize(Vector3::Cross(Vector3(1, 0, 0), pointingVector));


		if (Math::NearZero(axis.Length()))
		{
			mQuat = Quaternion::Identity;
		}
		
		else
		{
			mQuat = Quaternion(axis, angle);
		}
	}
	SetPosition(mGame->GetRenderer()->Unproject(Vector3(0.0f, 250.0f, 0.1f)));
}

