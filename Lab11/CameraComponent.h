#pragma once
#include "Component.h"
#include "Math.h"

class CameraComponent : public Component
{
public:
	float mPitchAngle = 0;
	float mPitchSpeed = 0;

	CameraComponent(class Actor* owner);
	void Update(float deltaTime) override;
	void SetPitchSpeed(float newSpeed) { mPitchSpeed = newSpeed; }
	float GetPitchSpeed() { return mPitchSpeed; }
};