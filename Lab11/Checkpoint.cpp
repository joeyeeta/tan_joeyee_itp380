#include "Player.h"
#include "Actor.h"
#include "Game.h"
#include "Renderer.h"
#include "MeshComponent.h"
#include "CollisionComponent.h"
#include "CameraComponent.h"
#include "Checkpoint.h"
#include "SDL/SDL_mixer.h"

Checkpoint::Checkpoint(Game * game)
	:Actor(game)
{
	mMesh = new MeshComponent(this);
	mMesh->SetMesh(mGame->GetRenderer()->GetMesh("Assets/Checkpoint.gpmesh"));
	mMesh->SetTextureIndex(1); //default grey

	mCol = new CollisionComponent(this);
	mCol->SetSize(25, 25, 25); 

	mGame->mCheckpointQueue.push(this);
}

void Checkpoint::UpdateActor(float deltaTime)
{
	//If next checkpoint
	if (this == mGame->mCheckpointQueue.front())
	{
		mActive = true;
	}

	if (mActive)
	{
		mMesh->SetTextureIndex(0);
	}

	//If collide with player
	if (mCol->Intersect(mGame->mPlayer->mCol))
	{
		if (mLevelString != "")
		{
			mGame->mNextLevel = mLevelString;
		}

		Mix_PlayChannel(-1, mGame->GetSound("Assets/Sounds/Checkpoint.wav"), 0);
		mGame->mPlayer->SetRespawnPos(this->GetPosition());
		SetState(EDead);
		mGame->mCheckpointQueue.pop();
	}
}
