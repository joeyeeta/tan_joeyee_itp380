#include "Player.h"
#include "Actor.h"
#include "Game.h"
#include "Renderer.h"
#include "MeshComponent.h"
#include "CollisionComponent.h"
#include "Coin.h"
#include "Checkpoint.h"
#include "SDL/SDL_mixer.h"

Coin::Coin(Game * game)
	:Actor(game)
{
	mMesh = new MeshComponent(this);
	mMesh->SetMesh(game->GetRenderer()->GetMesh("Assets/Coin.gpmesh"));
	mCol = new CollisionComponent(this);
	mCol->SetSize(100, 100, 100);
}

void Coin::UpdateActor(float deltaTime)
{
	SetRotation(GetRotation() + Math::Pi * deltaTime);

	if (mCol->Intersect(mGame->mPlayer->mCol))
	{
		Mix_PlayChannel(-1, mGame->GetSound("Assets/Sounds/Coin.wav"), 0);
		SetState(EDead);
	}
}

