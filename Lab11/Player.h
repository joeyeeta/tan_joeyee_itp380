#pragma once
#include <vector>
#include <SDL/SDL_stdinc.h>
#include "Math.h"
#include "Actor.h"

class Player : public Actor
{
public:
	Vector3 mRespawn;

	Player(class Game* game);
	void SetRespawnPos(Vector3 pos);
};
