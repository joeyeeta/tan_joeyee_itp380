#pragma once
#include <vector>
#include <SDL/SDL_stdinc.h>
#include "Math.h"
#include "Actor.h"

class Block : public Actor
{
public:
	Block(class Game* game);
	~Block();
};
