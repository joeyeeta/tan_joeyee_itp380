#pragma once
#include <vector>
#include <SDL/SDL_stdinc.h>
#include "Math.h"
#include "Actor.h"

class Checkpoint : public Actor
{
public:
	bool mActive = false;
	std::string mLevelString;
	std::string mText;

	Checkpoint(class Game* game);
	void UpdateActor(float deltaTime) override;
	void SetLevelString(std::string levelName) { mLevelString = levelName; }
};
