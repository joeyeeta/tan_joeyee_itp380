#include "HUD.h"
#include "Texture.h"
#include "Shader.h"
#include "Game.h"
#include "Renderer.h"
#include "Font.h"
#include "Checkpoint.h"
#include <sstream>
#include <iomanip>

HUD::HUD(Game* game)
	:mGame(game)
	,mFont(nullptr)
{
	// Load font
	mFont = new Font();
	mFont->Load("Assets/Inconsolata-Regular.ttf");
	mTimerText = mFont->RenderText("00:00.00");
	mCoinText = mFont->RenderText("0/55");
}

HUD::~HUD()
{
	// Get rid of font
	if (mFont)
	{
		mFont->Unload();
		delete mFont;
	}
}

void HUD::Update(float deltaTime)
{
	// TODO
	mTime += 1 * deltaTime;

	mTimerText->Unload();
	delete mTimerText;

	ff += deltaTime * 100;

	if (ff >= 100)
	{
		ff = 0;
	}

	ss += deltaTime;

	if (ss >= 60)
	{
		ss = 0;
		mm += 1;
	}

	std::string mmString;
	std::string ssString;
	std::string ffString;

	if (ff < 10)
	{
		ffString += "0";
	}

	if (ss < 10)
	{
		ssString += "0";
	}

	if (mm < 10)
	{
		mmString += "0";
	}

	mmString += std::to_string(mm);
	ssString += std::to_string((int)ss);
	ffString += std::to_string((int)ff);

	std::string timerString = mmString + std::string(":") + ssString + std::string(".") + ffString;

	mTimerText = mFont->RenderText(timerString);
}

void HUD::Draw(Shader* shader)
{
	// TODO
	DrawTexture(shader, mTimerText, Vector2(-420.0f, -325.0f));
	DrawTexture(shader, mCoinText, Vector2(-450.0f, -290.0f));

	if (mTime < mCheckpointTimer)
	{
		DrawTexture(shader, mCheckpointText, Vector2::Zero);
	}
}

void HUD::AddCoin()
{
	mCoins += 1; 
	mCoinText = mFont->RenderText(std::to_string(mCoins) + "/55");	
}

void HUD::CheckpointText(Checkpoint* check)
{
	mCheckpointText = mFont->RenderText(check->mText);
	mCheckpointTimer = mTime + 5;
}

void HUD::DrawTexture(class Shader* shader, class Texture* texture,
				 const Vector2& offset, float scale)
{
	// Scale the quad by the width/height of texture
	Matrix4 scaleMat = Matrix4::CreateScale(
		static_cast<float>(texture->GetWidth()) * scale,
		static_cast<float>(texture->GetHeight()) * scale,
		1.0f);
	// Translate to position on screen
	Matrix4 transMat = Matrix4::CreateTranslation(
		Vector3(offset.x, offset.y, 0.0f));	
	// Set world transform
	Matrix4 world = scaleMat * transMat;
	shader->SetMatrixUniform("uWorldTransform", world);
	// Set current texture
	texture->SetActive();
	// Draw quad
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);
}
