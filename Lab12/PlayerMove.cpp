#include "PlayerMove.h"
#include "Math.h"
#include "CameraComponent.h"
#include "CollisionComponent.h"
#include "Actor.h"
#include "Player.h"
#include "Game.h"
#include "Block.h"
#include <vector>
#include <algorithm>

PlayerMove::PlayerMove(Actor * owner)
	:MoveComponent(owner)
{
	ChangeState(Falling);
	mRunningSFX = Mix_PlayChannel(-1, mOwner->GetGame()->GetSound("Assets/Sounds/Running.wav"), -1);
	Mix_Pause(mRunningSFX);
}

PlayerMove::~PlayerMove()
{
	Mix_HaltChannel(mRunningSFX);
}

void PlayerMove::FixXYVelocity()
{
	Vector2 tempVelocity = Vector2(mVelocity.x, mVelocity.y);
	float length = sqrt(tempVelocity.x * tempVelocity.x + tempVelocity.y * tempVelocity.y);

	if (length > 400)
	{
		tempVelocity.x = (tempVelocity.x / length) * 400;
		tempVelocity.y = (tempVelocity.y / length) * 400;
	}

	if ((mCurrentState == OnGround) || (mCurrentState == WallClimb))
	{
		if (Math::NearZero(mAcceleration.x))
		{
			tempVelocity.x = tempVelocity.x * 0.9;
		}
		if (Math::NearZero(mAcceleration.y))
		{
			tempVelocity.y = tempVelocity.y * 0.9;
		}

		if (((mAcceleration.x > 0) && (tempVelocity.x < 0)) || ((mAcceleration.x < 0) && (tempVelocity.x > 0)))
		{
			if (Math::NearZero(mAcceleration.x))
			{
				tempVelocity.x = tempVelocity.x * 0.9;
			}
		}

		if (((mAcceleration.y > 0) && (tempVelocity.y < 0)) || ((mAcceleration.y < 0) && (tempVelocity.y > 0)))
		{
			if (Math::NearZero(mAcceleration.y))
			{
				tempVelocity.y = tempVelocity.y * 0.9;
			}
		}
	}

	mVelocity.x = tempVelocity.x;
	mVelocity.y = tempVelocity.y;
}

void PlayerMove::PhysicsUpdate(float deltaTime)
{
	mAcceleration = mPendingForces * (1 / mMass);
	mVelocity += mAcceleration * deltaTime;
	FixXYVelocity();
	mOwner->SetPosition(mOwner->GetPosition() + mVelocity * deltaTime);
	mOwner->SetRotation((GetAngularSpeed() * deltaTime) + mOwner->GetRotation());
	mPendingForces = Vector3::Zero;
}

void PlayerMove::AddForce(const Vector3 & force)
{
	mPendingForces += force;
}

void PlayerMove::Update(float deltaTime)
{
	if (mCurrentState == OnGround)
	{
		UpdateOnGround(deltaTime);
		Mix_Pause(mRunningSFX);

		if (mVelocity.Length() > 50)
		{
			Mix_Resume(mRunningSFX);
		}
	}

	else if (mCurrentState == Jump)
	{
		UpdateJump(deltaTime);
		Mix_Pause(mRunningSFX);
	}

	else if (mCurrentState == Falling)
	{
		UpdateFalling(deltaTime);
		Mix_Pause(mRunningSFX);
	}

	else if (mCurrentState == WallClimb)
	{
		UpdateWallClimb(deltaTime);
		Mix_Resume(mRunningSFX);
	}

	else if (mCurrentState == WallRun)
	{
		UpdateWallRun(deltaTime);
		Mix_Resume(mRunningSFX);
	}

	if (mOwner->GetPosition().z < -750)
	{
		mOwner->SetPosition(static_cast<Player*>(mOwner)->mRespawn);
		mOwner->SetRotation(0);
		mVelocity = Vector3::Zero;
		mPendingForces = Vector3::Zero;
		ChangeState(Falling);
	}
}

void PlayerMove::ProcessInput(const Uint8 * keyState)
{
	int x, y;
	SDL_GetRelativeMouseState(&x, &y);

	float newX = (float)x;
	float newY = (float)y;
	SetAngularSpeed(Math::Pi * 10 * (newX / 500));
	mOwner->mCamera->SetPitchSpeed(Math::Pi * 10 * (newY / 500));

	if (keyState[SDL_SCANCODE_W])
	{
		AddForce(mOwner->GetForward() * 700);
	}

	else if (keyState[SDL_SCANCODE_S])
	{
		AddForce(mOwner->GetForward() * -700);
	}

	else if (!(keyState[SDL_SCANCODE_W]) && !(keyState[SDL_SCANCODE_S]))
	{
		AddForce(Vector3::Zero);
	}

	else if ((keyState[SDL_SCANCODE_W]) && (keyState[SDL_SCANCODE_S]))
	{
		AddForce(Vector3::Zero);
	}

	if (keyState[SDL_SCANCODE_D])
	{
		AddForce(mOwner->GetRight() * 700);
	}

	else if (keyState[SDL_SCANCODE_A])
	{
		AddForce(mOwner->GetRight() * -700);
	}

	else if (!(keyState[SDL_SCANCODE_D]) && !(keyState[SDL_SCANCODE_A]))
	{
		SetStrafeSpeed(0);
	}

	else if ((keyState[SDL_SCANCODE_D]) && (keyState[SDL_SCANCODE_A]))
	{
		SetStrafeSpeed(0);
	}

	if (keyState[SDL_SCANCODE_SPACE])
	{
		if ((mSpacedPressed) && (mCurrentState == OnGround))
		{
			ChangeState(Jump);
			AddForce(mJumpForce);
			Mix_PlayChannel(-1, mOwner->GetGame()->GetSound("Assets/Sounds/Jump.wav"), 0);
		}

		mSpacedPressed = true;
	}

	else
	{
		mSpacedPressed = false;
	}
}

void PlayerMove::ChangeState(MoveState newState)
{
	mCurrentState = newState;
}

void PlayerMove::UpdateOnGround(float deltaTime)
{
	PhysicsUpdate(deltaTime);
	bool falling = true;

	for (int i = 0; i < mOwner->GetGame()->mBlockVector.size(); i++)
	{
		CollSide collside = FixCollision(mOwner->mCol, mOwner->GetGame()->mBlockVector[i]->mCol);
		if (collside == Top)
		{
			falling = false;
		}
		
		if ((collside == SideX1) || (collside == SideX2) || (collside == SideY1) || (collside == SideY2))
		{
			if (CanWallClimb(FixCollision(mOwner->mCol, mOwner->GetGame()->mBlockVector[i]->mCol)))
			{
				mWallClimbTimer = 0;
				mCurrentState = WallClimb;
				return;
			}
		}

	}

	if (falling)
	{
		ChangeState(Falling);
	}
}

void PlayerMove::UpdateJump(float deltaTime)
{
	AddForce(mGravity);
	PhysicsUpdate(deltaTime);

	for (int i = 0; i < mOwner->GetGame()->mBlockVector.size(); i++)
	{
		CollSide collside = FixCollision(mOwner->mCol, mOwner->GetGame()->mBlockVector[i]->mCol);

		if (FixCollision(mOwner->mCol, mOwner->GetGame()->mBlockVector[i]->mCol) == Bottom)
		{
			mVelocity.z = 0;
			ChangeState(Falling);
		}

		if ((collside == SideX1) || (collside == SideX2) || (collside == SideY1) || (collside == SideY2))
		{
			if (CanWallClimb(FixCollision(mOwner->mCol, mOwner->GetGame()->mBlockVector[i]->mCol)))
			{
				mWallClimbTimer = 0;
				mCurrentState = WallClimb;
				return;
			}

			else if (CanWallRun(FixCollision(mOwner->mCol, mOwner->GetGame()->mBlockVector[i]->mCol)))
			{
				SDL_Log("canwallrun");
				mWallRunTimer = 0;
				ChangeState(WallRun);
				return;
			}
		}

		if (mVelocity.z <= 0)
		{
			ChangeState(Falling);
		}
	}
}

void PlayerMove::UpdateFalling(float deltaTime)
{
	AddForce(mGravity);
	PhysicsUpdate(deltaTime);

	for (int i = 0; i < mOwner->GetGame()->mBlockVector.size(); i++)
	{
		FixCollision(mOwner->mCol, mOwner->GetGame()->mBlockVector[i]->mCol);
		if (FixCollision(mOwner->mCol, mOwner->GetGame()->mBlockVector[i]->mCol) == Top)
		{
			Mix_PlayChannel(-1, mOwner->GetGame()->GetSound("Assets/Sounds/Land.wav"), 0);
			mVelocity.z = 0;
			ChangeState(OnGround);
		}
	}
}

void PlayerMove::UpdateWallClimb(float deltaTime)
{
	mWallClimbTimer += deltaTime;
	AddForce(mGravity);

	if (mWallClimbTimer < 0.4)
	{
		AddForce(mWallClimbForce);
	}

	PhysicsUpdate(deltaTime);

	bool collide = false;

	for (int i = 0; i < mOwner->GetGame()->mBlockVector.size(); i++)
	{
		CollSide collside = FixCollision(mOwner->mCol, mOwner->GetGame()->mBlockVector[i]->mCol);

		if ((collside == SideX1) || (collside == SideX2) || (collside == SideY1) || (collside == SideY2))
		{
			collide = true;
		}
	}

	if ((collide == false) || (mVelocity.z <= 0))
	{
		mVelocity.z = 0;
		ChangeState(Falling);
	}

}

void PlayerMove::UpdateWallRun(float deltaTime)
{
	mWallRunTimer += deltaTime;
	AddForce(mGravity);

	if (mWallRunTimer < 0.4)
	{
		AddForce(mWallRunForce);
	}

	PhysicsUpdate(deltaTime);

	bool collide = false;

	for (int i = 0; i < mOwner->GetGame()->mBlockVector.size(); i++)
	{
		CollSide collside = FixCollision(mOwner->mCol, mOwner->GetGame()->mBlockVector[i]->mCol);

		if ((collside == SideX1) || (collside == SideX2) || (collside == SideY1) || (collside == SideY2))
		{
			collide = true;
		}
	}

	if (mVelocity.z <= 0)
	{
		mVelocity.z = 0;
		ChangeState(Falling);
	}

}

bool PlayerMove::CanWallClimb(CollSide side)
{
	bool facingSide = false;
	bool xyMoving = false;
	bool xyHigh = false;

	float playerMag = (sqrt(mOwner->GetForward().x * mOwner->GetForward().x + mOwner->GetForward().y * mOwner->GetForward().y + mOwner->GetForward().z * mOwner->GetForward().z));
	Vector3 playerNormal = Vector3(mOwner->GetForward().x / playerMag, mOwner->GetForward().y / playerMag, mOwner->GetForward().z / playerMag);
	
	float xyMag = sqrt(mVelocity.x * mVelocity.x + mVelocity.y * mVelocity.y);
	Vector2 xyNormal = Vector2(mVelocity.x / xyMag, mVelocity.y / xyMag);

	Vector3 sideNormal;

	if (side == SideX1)
	{
		sideNormal = Vector3(1, 0, 0);
	}

	else if (side == SideX2)
	{
		sideNormal = Vector3(-1, 0, 0);
	}

	else if (side == SideY1)
	{
		sideNormal = Vector3(0, 1, 0);
	}

	else if (side == SideY2)
	{
		sideNormal = Vector3(0, -1, 0);
	}

	//if facing towards side of block
	float angle = atan2(playerNormal.y, playerNormal.x) - atan2(sideNormal.y, sideNormal.x);
	float dotProduct = (playerNormal.x * playerNormal.x + playerNormal.y * playerNormal.y + playerNormal.z * playerNormal.z) * (sideNormal.x * sideNormal.x + sideNormal.y * sideNormal.y + sideNormal.z * sideNormal.z) * Math::Cos(angle);

	if ((dotProduct > 0.9) && (dotProduct <= 1))
	{
		facingSide = true;
	}

	//if movement towards side of block
	float angle2 = atan2(xyNormal.y, xyNormal.x) - atan2(sideNormal.y, sideNormal.x);
	float dotProduct2 = (xyNormal.x * xyNormal.x + xyNormal.y * xyNormal.y) * (sideNormal.x * sideNormal.x + sideNormal.y * sideNormal.y) * Math::Cos(angle2);

	if ((dotProduct2 > 0.9) && (dotProduct2 <= 1))
	{
		xyMoving = true;
	}

	//if xy velocity is high enough
	if (xyMag > 350)
	{
		xyHigh = true;
	}

	//Check if all conditions met
	if ((facingSide) && (xyHigh) && (xyMoving))
	{
		return true;
	}

	else
	{
		return false;
	}

}

bool PlayerMove::CanWallRun(CollSide side)
{
	bool retVal = false;

	// I can wall run if:
	// I'm perpendicular to wall
	// My velocity is roughly forward
	// My xy velocity is high enough

	Vector3 sideNormal;

	if (side == SideX1)
	{
		sideNormal = Vector3(1, 0, 0);
	}

	else if (side == SideX2)
	{
		sideNormal = Vector3(-1, 0, 0);
	}

	else if (side == SideY1)
	{
		sideNormal = Vector3(0, 1, 0);
	}

	else if (side == SideY2)
	{
		sideNormal = Vector3(0, -1, 0);
	}

	Vector3 velocityDir = Vector3(mVelocity.x, mVelocity.y, 0.0f);
	velocityDir.Normalize();
	Vector2 xyVelocity(mVelocity.x, mVelocity.y);
	float dotSide = Vector3::Dot(sideNormal, mOwner->GetRight());
	float dotFwd = Vector3::Dot(mOwner->GetForward(), velocityDir);
	if (Math::Abs(dotSide) > 0.7f &&
		dotFwd > 0.9f &&
		xyVelocity.Length() > 350.0f)
	{
		retVal = true;
	}

	return retVal;
}

PlayerMove::CollSide PlayerMove::FixCollision(CollisionComponent * self, CollisionComponent * block)
{
	Vector3 pos = mOwner->GetPosition();

	if (self->Intersect(block))
	{
		// Get player min/max and block min/max
		Vector3 playerMin = self->GetMin();
		Vector3 playerMax = self->GetMax();
		Vector3 blockMin = block->GetMin();
		Vector3 blockMax = block->GetMax();

		// Figure out which side we are closest to
		float dx1 = blockMin.x - playerMax.x;
		float dx2 = blockMax.x - playerMin.x;
		float dy1 = blockMin.y - playerMax.y;
		float dy2 = blockMax.y - playerMin.y;
		float dz1 = blockMin.z - playerMax.z;
		float dz2 = blockMax.z - playerMin.z;

		float dx = 0.0f;
		if (Math::Abs(dx1) < Math::Abs(dx2))
		{
			dx = dx1;
		}
		else
		{
			dx = dx2;
		}

		float dy = 0.0f;
		if (Math::Abs(dy1) < Math::Abs(dy2))
		{
			dy = dy1;
		}
		else
		{
			dy = dy2;
		}

		float dz = 0.0f;
		if (Math::Abs(dz1) < Math::Abs(dz2))
		{
			dz = dz1;
		}
		else
		{
			dz = dz2;
		}

		// Are we closer to top or bottom?
		if (Math::Abs(dy) < Math::Abs(dx))
		{
			if (Math::Abs(dy) < Math::Abs(dz))
			{
				pos.y += dy;
				mOwner->SetPosition(pos);

				if (dy == dy1)
				{
					AddForce(Vector3(0, -700, 0));
					return SideY1;
				}
				
				else
				{
					AddForce(Vector3(0, 700, 0));
					return SideY2;
				}
			}

			else
			{
				pos.z += dz;
				mOwner->SetPosition(pos);

				if (dz == dz1)
				{
					return Bottom;
				}

				else
				{
					return Top;
				}
			}
		}
		else
		{
			if (Math::Abs(dx) < Math::Abs(dz))
			{
				pos.x += dx;
				mOwner->SetPosition(pos);
				
				if (dx == dx1)
				{
					AddForce(Vector3(-700, 0, 0));
					return SideX1;
				}

				else
				{
					AddForce(Vector3(700, 0, 0));
					return SideX2;
				}
			}

			else
			{
				pos.z += dz;
				mOwner->SetPosition(pos);

				if (dz == dz1)
				{
					return Bottom;
				}

				else
				{
					return Top;
				}
			}
		}
	}

	else
	{
		return None;
	}
}


